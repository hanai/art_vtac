import os, inspect
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(os.path.dirname(currentdir))
os.sys.path.insert(0, parentdir)

import time
import pybullet as p
import pybullet_data
import numpy as np
import os
import random

import tp_util.transformations as t
from tp_util.sampling import *

# from kukaCamGymEnv import KukaCamGymEnv
# environment = KukaCamGymEnv(renders=True, isDiscrete=False)
# r = environment._kuka
# uid = r.kukaUid

cid = p.connect(p.SHARED_MEMORY)
if (cid < 0):
    p.connect(p.GUI, options="--opengl2")
    #p.connect(p.GUI)
p.resetDebugVisualizerCamera(1.3, 180, -41, [0.52, -0.2, -0.33])

urdfRoot = '/home/ryo/Program/art_vtac/script/pybullet/rspt_ur5e_description'
robotUid = p.loadURDF(os.path.join(urdfRoot, 'ur5e_robotiq.urdf'))

def setupEnv():
    p.loadURDF(os.path.join(pybullet_data.getDataPath(), "plane.urdf"), [0, 0, -0.8])
    p.loadURDF(os.path.join(pybullet_data.getDataPath(), "table/table.urdf"),
                0.5000000, 0.00000, -.620000, 0.000000, 0.000000, 0.0, 1.0)

# joint name -> id mapping for ur5e with robotiq gripper
armJointIds = {
    'shoulder_pan_joint' : 1,
    'shoulder_lift_joint' : 2,
    'elbow_joint' : 3,
    'wrist_1_joint' : 4,
    'wrist_2_joint' : 5,
    'wrist_3_joint' : 6
    }

gripperJointIds = {
    'hand_right_driver_joint' : 10,
    'hand_right_follower_joint' : 12,
    'hand_right_spring_link_joint' : 14,
    'hand_left_driver_joint' : 15,
    'hand_left_follower_joint' : 17,
    'hand_left_spring_link_joint' : 19
    }

def setJointPositions(jointIndices, jointPositions, maxForce):
    for jointIndex, v in zip(jointIndices, jointPositions):
        p.setJointMotorControl2(robotUid,
                                jointIndex,
                                p.POSITION_CONTROL,
                                targetPosition=v,
                                force=maxForce)

def setJ(jointPositions, maxForce=100):
    setJointPositions(armJointIds.values(), jointPositions, maxForce)

def setG(jointPosition=0.0, maxForce=100):
    if jointPosition < 0.0 or jointPosition > 0.8726:
        print('out of joints limits')
        return
    setJointPositions(gripperJointIds.values(),
                          [jointPosition, -jointPosition, jointPosition,
                               jointPosition, -jointPosition, jointPosition],
                          maxForce)

def setL(pos, orn):
    '''
    orn : x,y,z,w
    ik returns all the joint values including gripper joints.
    '''
    jointPositions = p.calculateInverseKinematics(robotUid, armJointIds['wrist_3_joint'],
                                                  pos,
                                                  orn)
    setJ(jointPositions)

def goInitial():
    setJ([3.3, -2.1, -1.7, -0.8, 1.6, 0.07])
    setG(0.0)
    steps(n=500)

def getJointPositions():
    return [p.getJointState(robotUid, i)[0] for i in armJointIds.values()]

def getGripperJointPosition():
    return p.getJointState(robotUid, gripperJointIds['hand_right_driver_joint'])[0]

def steps(n=300):
    for i in range(n):
        p.stepSimulation()

def putObject():
    # xpos = 0.5 + 0.2 * random.random()
    # ypos = 0 + 0.25 * random.random()
    # ang = 3.1415925438 * random.random()
    ang = 0
    orn = p.getQuaternionFromEuler([0, 0, ang])
    # blockUid = p.loadURDF(os.path.join(pybullet_data.getDataPath(), "block.urdf"),
    #                           xpos, ypos, -0.1, orn[0], orn[1], orn[2], orn[3])
    objUid = p.loadURDF(os.path.join(pybullet_data.getDataPath(), "teddy_vhacd.urdf"),
                        0.5, 0, 0.02, orn[0], orn[1], orn[2], orn[3])
    return objUid

setupEnv()
goInitial()
objUid = putObject()

# Twrist = pq2mat(*p.getLinkState(robotUid, armJointIds['wrist_3_joint'])[4:6])
# Twrist_obj = np.dot(t.inverse_matrix(Tobj), Twrist)

F0 = np.array([
    [ 0, 0, 1,     0],
    [-1, 0, 0,     0],
    [ 0, -1,0, 0.325],
    [ 0, 0, 0, 1.   ]
    ])
F1 = np.array([
    [ 0, 0, 1,     0],
    [-1, 0, 0,     0],
    [ 0, -1,0, 0.255],
    [ 0, 0, 0, 1.   ]
    ])
F2 = np.array([
    [ 0, 0, 1,     0],
    [-1, 0, 0,     0],
    [ 0, -1,0, 0.240],
    [ 0, 0, 0, 1.   ]
    ])
traj = [F0,F1,F2]

def grasp(traj):
    def moveTo(T):
        Tobj = pq2mat(*p.getBasePositionAndOrientation(objUid))
        Twrist = np.dot(Tobj, T)
        p_wrist = t.translation_from_matrix(Twrist)
        q_wrist = t.quaternion_from_matrix(Twrist)
        setL(p_wrist, q_wrist)
        steps()

    for wp in traj:
        moveTo(wp)

    setG(0.1)
    steps()
    setG(0.2)
    steps()
    setG(0.3)
    for i in range(3):
        steps()


def showFrame(F, lifeTime=5.0):
    '''
    hand_right_pad_joint -> hand_right_pad
    '''
    origin = F[0:3,3]
    l = 0.2
    ex = np.dot(F, [l,0,0,1])[:3]
    ey = np.dot(F, [0,l,0,1])[:3]
    ez = np.dot(F, [0,0,l,1])[:3]
    p.addUserDebugLine(origin, ex, lineColorRGB=[1,0,0], lifeTime=lifeTime)
    p.addUserDebugLine(origin, ey, lineColorRGB=[0,1,0], lifeTime=lifeTime)
    p.addUserDebugLine(origin, ez, lineColorRGB=[0,0,1], lifeTime=lifeTime)
    # itemIdx = p.addUserDebugLine(startPoint, endPoint)
    # p.removeUserDebugItem(itemIdx)


Tfinger = pq2mat(*p.getLinkState(robotUid, gripperJointIds['hand_left_follower_joint'])[4:6])
Tfinger_cam = t.translation_matrix([0, -0.03, 0.03])
Tcam = np.dot(Tfinger, Tfinger_cam)
cameraEyePosition = Tcam[0:3,3]
cameraTargetFrame = np.dot(Tcam, t.translation_matrix([0, -0.1, 0]))
cameraTargetPosition = cameraTargetFrame[0:3,3]
cameraUpFrame = np.dot(Tcam, t.translation_matrix([0, 0, -0.1]))
cameraUpVector = cameraUpFrame[0:3,3]
viewMatrix = p.computeViewMatrix(cameraEyePosition, cameraTargetPosition, cameraUpVector)
# viewMat = p.computeViewMatrix([0,0,0],[1,0,0],[0,0,1])

# projMatrix = p.computeProjectionMatrix(left=0, right=160, bottom=0, top=120, nearVal=0.1, farVal=0.5)
projMatrix = [
    0.75, 0.0, 0.0, 0.0,
    0.0, 1.0, 0.0, 0.0,
    0.0, 0.0, -1.0000200271606445, -1.0,
    0.0, 0.0, -0.02000020071864128, 0.0
    ]

_,_,rgba,depth = p.getCameraImage(320, 240, viewMatrix, projMatrix)

def show_image(img, title='image'):
    if type(img) == np.ndarray:
        plt.figure()
        plt.title(title)
        if img.dtype == 'uint8':
            plt.imshow(img)
        else:
            plt.imshow(img.astype('uint8'))
        plt.show()
    elif type(img) == Image.Image:
        img.show(title=title) # Not all viewers support the title
    else:
        Exception('unknow image type')

# show_image(rgba)
