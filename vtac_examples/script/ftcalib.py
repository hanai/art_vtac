#!/usr/bin/env python

import sys
import socket
import rospy
from geometry_msgs.msg import WrenchStamped, Wrench
import numpy as np
from vtac_examples.srv import set_zero
import copy

# pub = rospy.Publisher('/d_bot_controller/wrench_calibrated', WrenchStamped, queue_size=1)
pub = rospy.Publisher('wrench_calibrated', WrenchStamped, queue_size=1)
# host = rospy.get_param('~host', '192.168.13.101')
# port = rospy.get_param('~port', 63351)

initialized = False
offset = Wrench()
last_msg = Wrench()

def set_zero_callback(val):
    print(val)
    offset.force.x = last_msg.force.x
    offset.force.y = last_msg.force.y
    offset.force.z = last_msg.force.z
    offset.torque.x = last_msg.torque.x
    offset.torque.y = last_msg.torque.y
    offset.torque.z = last_msg.torque.z
    return True

def callback(in_msg):
    global last_msg
    global initialized
    out_msg = WrenchStamped()
    out_msg.header.stamp = in_msg.header.stamp
    if in_msg.header.frame_id == '':
        frame_id = rospy.get_param('~frame_id', 'robotiq_ft_frame_id')
    else:
        frame_id = in_msg.header.frame_id
    out_msg.header.frame_id = frame_id
    out_msg.wrench.force.x = in_msg.wrench.force.x - offset.force.x
    out_msg.wrench.force.y = in_msg.wrench.force.y - offset.force.y
    out_msg.wrench.force.z = in_msg.wrench.force.z - offset.force.z
    out_msg.wrench.torque.x = in_msg.wrench.torque.x - offset.torque.x
    out_msg.wrench.torque.y = in_msg.wrench.torque.y - offset.torque.y
    out_msg.wrench.torque.z = in_msg.wrench.torque.z - offset.torque.z
    last_msg.force.x = in_msg.wrench.force.x
    last_msg.force.y = in_msg.wrench.force.y
    last_msg.force.z = in_msg.wrench.force.z
    last_msg.torque.x = in_msg.wrench.torque.x
    last_msg.torque.y = in_msg.wrench.torque.y
    last_msg.torque.z = in_msg.wrench.torque.z
    if not initialized:
        set_zero_callback(True)
        initialized = True
    # rospy.loginfo(str)
    pub.publish(out_msg)


def main():
    rospy.init_node('ftcalib')
    # rospy.Subscriber('/d_bot_controller/wrench', WrenchStamped, callback)
    rospy.Subscriber('wrench', WrenchStamped, callback)
    rospy.Service('set_zero', set_zero, set_zero_callback)
    rospy.spin()

if __name__ == '__main__':
    try:
        main()
    except rospy.ROSInterruptException:
        pass


# echo 'set ZERO to force torque sensors'
# rosservice call /right_arm/robotiq_force_torque_sensor_acc "SET ZRO"
# rosservice call /left_arm/robotiq_force_torque_sensor_acc "SET ZRO"
