#!/usr/bin/env python

import cv2
import os
import sys

class Screw:
    def __init__(self, head, size, length):
        self._head = head
        self._size = size
        self._length = length
    @property
    def head(self):
        return self._head
    @property
    def size(self):
        return self._size
    @property
    def length(self):
        return self._length

    def __str__(self):
        return '{}_{}_{}'.format(self.head, self.size, self.length)

screws = [
    Screw('hex', 6, 20),
    Screw('hex', 6, 15),
    Screw('flat-plus', 5, 20),
    Screw('pan-plus', 5, 15)
    ]

def save_frame_camera(screw,
                          pose,
                          with_head,
                          basename = 'img',
                          ext = 'png',
                          delay = 1,
                          window_name = 'frame',
                          device_num = 0):
    cap = cv2.VideoCapture(device_num)

    if not cap.isOpened():
        return

    dir_path = '{}_{}_{}'.format(screw, pose, with_head)
    os.makedirs(dir_path)
    base_path = os.path.join(dir_path, basename)

    n = 0
    while True:
        ret, frame = cap.read()
        cv2.imshow(window_name, frame)
        key = cv2.waitKey(delay) & 0xFF
        if key == ord('c'):
            cv2.imwrite('{}_{}.{}'.format(base_path, n, ext), frame)
            n += 1
        elif key == ord('q'):
            break

    cv2.destroyWindow(window_name)


if __name__ == '__main__':
    '''
    0: up, 1: down
    0: head not included, 1: head included
    '''
    if len(sys.argv) < 4:
        print('./record.py 0 0 0')
    else:
        save_frame_camera(screws[int(sys.argv[1])], int(sys.argv[2]), int(sys.argv[3]))
