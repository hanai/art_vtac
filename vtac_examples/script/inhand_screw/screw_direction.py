#!/usr/bin/env python3

# from visualization import *

import numpy as np
import time, math, datetime

from keras.models import load_model
from keras.optimizers import Adam
from keras.callbacks import TensorBoard, ModelCheckpoint

import dataset, vtac_data, generator
from PIL import Image

class ScrewDirectionDataset(dataset.Dataset):
    """
    """

    tags = ['vtac_image']

    def __init__(self, with_head=True,
                     random_train_test_split=False,
                     train_objects=[],
                     test_objects=[],
                     seed=1337):
        self.reset()
        self._with_head = with_head
        self.scan_files()
        data_ids = list(self._table.values())[0]
        self.preprocess()
        super().__init__(data_ids, random_train_test_split, train_objects, test_objects, seed=seed)

    def scan_files(self):
        self.reset()
        self._data = vtac_data.load_data('vtacdata190807.h5')

        if self._with_head:
            self._data = [d for d in self._data if d['with_head'] == True]

        for data_id,d in enumerate(self._data):
            name = d['screw_name']
            try:
                self._table[name].append(data_id)
            except:
                self._table[name] = [data_id]

    def preprocess(self):
        print('preprocessing [crop, resize, ...]')
        for dids in self._table.values():
            for did in dids:
                d = self.load(did)
                img = d['vtac_image']
                pimg = Image.fromarray(img)
                cropped_img = pimg.crop((80, 0, 560, 480))
                resized_img = cropped_img.resize((256, 256))
                d['vtac_image'] = np.asarray(resized_img)

    def get_object_name_by_id(self, data_id):
        return self._data[data_id]['screw_name']

    def load(self, data_id):
        """
        @param data_id Integer in [0,120]
        """
        return self._data[data_id]


def problem(number=0):
    def rotate(l,m):
        return l[-m:]+l[:-m]
    objects = ['hex_6_20', 'flat-plus_5_20', 'pan-plus_5_15']
    l = rotate(objects, number)
    n = (len(l) + 1) // 2
    train,test = l[:n],l[n:]
    print('train_objects = ', train)
    print('test_objects = ', test)
    return train,test


def screw_direction(model, xshape, problem_number, batch_size=16,
                    n_validation_batches=30, n_epochs=50, rescale=1./255, seed=1337,
                    lr=1e-5):
    prefix = model.name
    tags = ['vtac_image']
    train_objects,test_objects = problem(problem_number)

    def logdir(prefix, batch_size, seed, rescale):
        now_str = datetime.datetime.now().strftime('%y%m%d%H%M%S')
        r_suffix = 'n' if rescale == None else 'r'
        return f'./log/{prefix}-bs{batch_size:02d}-sd{seed:05d}-{r_suffix}-{now_str}'

    ds = ScrewDirectionDataset(train_objects=train_objects, test_objects=test_objects, seed=seed)
    vtg = generator.VisTacGenerator(horizontal_flip=True,
                                        random_crop=list(xshape[:2]),
                                        rescale=rescale,
                                        y_generator=lambda e: e['direction'])
    train_gen = vtg.flow_from_HDFs(ds, ds.train_ids(), tags=tags, batch_size=batch_size)
    test_gen = vtg.flow_from_HDFs(ds, ds.test_ids(), tags=tags, batch_size=batch_size)
    model.compile(loss='binary_crossentropy', optimizer=Adam(lr=lr), metrics=['binary_accuracy'])
    model.summary()

    tb_cb = TensorBoard(log_dir=logdir(prefix, batch_size, seed, rescale),
                            write_graph=True,
                            write_images=1,
                            )
    md_cb = ModelCheckpoint("model.hdf5",
                            monitor='val_loss',
                            verbose=1,
                            save_best_only=True,
                            save_weights_only=False)

    # get several batches and merge them
    batches = []
    for i in range(n_validation_batches):
        batches.append(next(test_gen))

    xs,ys = zip(*batches)
    batch_merged = (list(map(np.concatenate, zip(*xs))), np.concatenate(ys))
    # show_batch(batch_merged[0][0], title='validation data', dim=(12,12))

    model.fit_generator(train_gen, steps_per_epoch=train_gen.N//batch_size,
        # validation_data=test_gen, validation_steps=test_gen.N//batch_size,
        validation_data=batch_merged,
        epochs=n_epochs,
        callbacks=[tb_cb, md_cb]
        )


def false_positive(result, threshold):
    def is_false_positive(d, eps=1e-3):
        t,y,_ = d
        return t > threshold and y < eps
    return [x for x in result if is_false_positive(x)]

def show_result(ds, result, false_positive_only, threshold):
    if false_positive_only:
        result = false_positive(result, threshold)

    cols = 1
    dim = (len(result), cols)
    plt.figure(figsize=dim)

    def subplot(img, j):
        plt.subplot(dim[0], dim[1], j)
        plt.imshow(img)
        plt.axis('off')

    for i,d in enumerate(result):
        y,t,data_id = d
        e = ds.load(data_id)
        subplot(e['vtac_image'], cols*i+1)
        # plt.title(f'y={t[0]:.1f}, t={y[0]:.5f}, did={data_id}')
        plt.title(f'y={t[0]:.1f}, t={y[0]:.5f}, did={data_id}, {ds.get_object_name_by_id(data_id)}')

    plt.tight_layout()
    plt.subplots_adjust(top=0.98, bottom=0.02, wspace=0.3, hspace=0.3)
    plt.show()

def test(model, ds, gen, false_positive_only=False, threshold=0.5):
    x,y,data_id = gen.__next__(with_data_id=True)
    t = model.predict(x)
    result = list(zip(t,y,data_id))
    show_result(ds, result, false_positive_only=false_positive_only, threshold=threshold)

from visualization import *
from keras.models import load_model

def predict_screw_direction(modelfile='model.hdf5', problem_number=0,
                                batch_size=24, seed=1337, rescale=1./255):
    xshape = (224,224,3)
    tags = ['vtac_image']
    train_objects,test_objects = problem(problem_number)

    model = load_model(modelfile)

    ds = ScrewDirectionDataset(train_objects=train_objects, test_objects=test_objects, seed=seed)
    vtg = generator.VisTacGenerator(horizontal_flip=True,
                                        random_crop=list(xshape[:2]),
                                        rescale=rescale,
                                        y_generator=lambda e: e['direction'])
    test_gen = vtg.flow_from_HDFs(ds, ds.test_ids(), tags=tags, batch_size=batch_size)
    test(model, ds, test_gen)
