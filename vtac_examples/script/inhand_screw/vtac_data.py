__author__ = "Ryo Hanai <ryo.hanai@aist.go.jp>"
__status__ = ""
__version__ = ""
__date__ = ""

import os, copy
import os.path
import numpy as np
import deepdish as dd

from keras.preprocessing import image
from keras.utils import get_file
from PIL import ImageOps, Image


def create_archive_from_images(dir_path, out_file='vtacdata190807.h5'):
    """
    Create *.h5 archive from the image files.
    @param dir_path A directory path that contains subdirectories with image files.
     e.g. dir_path='/home/rsptuser/Downloads/vtacdata190807',
    @param out_file File name of the archive.

    @return None
    """

    def scan_sub_dirs(dir_path):
        return os.listdir(dir_path)

    def parse_directory_name(dir):
        head,size,length,direction,with_head = str.split(dir, '_')
        d = {
            'screw_name' : '{}_{}_{}'.format(head, size, length),
            'direction' : int(direction),
            'with_head' : int(with_head) == 1
            }
        return d

    def load_images(image_dir_path):
        image_files = os.listdir(image_dir_path)
        return [np.array(image.load_img(os.path.join(image_dir_path, f))) for f in image_files]

    data = []
    dirs = scan_sub_dirs(dir_path)

    for dir in dirs:
        d_tmplt = parse_directory_name(dir)
        imgs = load_images(os.path.join(dir_path, dir))
        for img in imgs:
            d = copy.copy(d_tmplt)
            d['vtac_image'] = img
            data.append(d)

    print('saving to', out_file)
    dd.io.save(out_file, data)


def create_archive_2019_09_17(dir_path, out_file='vtacdata190917.h5'):
    """
    Create *.h5 archive from the image files.
    @param dir_path A directory path that contains subdirectories with image files.
     e.g. dir_path='/home/rsptuser/Downloads'
      The following directory structure is expected.
       <dir_path>/2019-09-09/label.txt
                             my_photo-1.png
                             ...
                  2019-09-17/images/label.txt
                                    my_photo-1.png
                                    ...
                             extra/label.txt
                                   my_photo-1.png
                                   ...
    @param out_file File name of the archive.

    @return None
    """

    def read_labels(label_file, table, id_prefix):
        with open(label_file, 'r') as f:
            lines = f.readlines()
        for l in lines:
            n,y = parse_line(l)
            table['{}{}'.format(id_prefix,n)] = y
        return table

    def parse_line(l):
        n,ystr = l.strip().split()
        if ystr == 'r':
            y = 0
        elif ystr == 'l':
            y = 1
        elif ystr == 'rr':
            y = 2
        elif ystr == 'll':
            y = 3
        elif ystr == 'rl' or ystr == 'lr':
            y = 4
        else:
            y = -1
        return int(n), y

    table = {}
    dir_path1 = os.path.join(dir_path, '2019-09-09')
    dir_path2 = os.path.join(dir_path, '2019-09-17')
    read_labels(os.path.join(dir_path1, 'label.txt'), table, '09_')
    read_labels(os.path.join(dir_path2, 'images', 'label.txt'), table, '17_')
    read_labels(os.path.join(dir_path2, 'extra', 'label.txt'), table, '17e_')
    o_width = 1620
    l_offset = (1920 - o_width)/2

    data = []
    for key in table.keys():
        dno_s, fno_s = [s for s in key.split('_')]
        fno = int(fno_s)
        if dno_s == '09':
            filepath = os.path.join(dir_path, '2019-09-09/my_photo-%d.png'%fno)
        elif dno_s == '17':
            filepath = os.path.join(dir_path, '2019-09-17/images/my_photo-%d.png'%fno)
        elif dno_s == '17e':
            filepath = os.path.join(dir_path, '2019-09-17/extra/my_photo-%d.png'%fno)
        else:
            print('unexpected directory')
            continue

        print(filepath)
        y = table[key]
        if y >= 0:
            img = image.load_img(filepath)

            # crop the image
            cropped_img = img.crop((l_offset, 0, l_offset+o_width, 1080))
            # resize the image
            resized_img = cropped_img.resize((256, 256))

            d = {
                'id': key,
                'label' : y,
                'image' : np.array(resized_img)
                }
            data.append(d)
            if y == 0:
                ym = 1
            elif y == 1:
                ym = 0
            elif y == 2:
                ym = 3
            elif y == 3:
                ym = 2
            else:
                ym = y
            d_mirror = {
                'id': key+'m',
                'label': ym,
                'image' : np.array(ImageOps.mirror(resized_img))
                # mirror() to reverse right and left, flip() to reverse up and down
                }
            data.append(d_mirror)
    print('saving to', out_file)
    dd.io.save(out_file, data)

archives = {
    '190807' : ('vtacdata190807.h5', 'https://drive.google.com/uc?export=download&id=1AQbJsNppvdq6-b_RU2IqwptAfNpG-B9i'),
    '190917' : ('vtacdata190917.h5', 'https://drive.google.com/uc?export=download&id=1KNBqVlduGRgBmvO8wjyWvxFUx7mpZsRU')
    }

def load_data(key):
    """
    Load *.h5 archive from ~/.keras/datasets directory if the archive exists.
    If not this function download the archive from the Internet

    @param path An archive file name

    @return Loaded data

    """

    path, origin = archives[key]
    path = get_file(path, origin = origin)

    print('Loading file: %s' % path)
    data = dd.io.load(path)
    print("N data: %d" % len(data))
    return data
