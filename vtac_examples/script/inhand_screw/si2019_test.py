#!/usr/bin/env python3

__author__ = "Ryo Hanai <ryo.hanai@aist.go.jp>"
__status__ = ""
__version__ = ""
__date__ = ""

from si2019 import *
from vtac_common import trainer

import numpy as np
import keras
import keras.backend as K
from keras.models import Sequential
from keras.layers import Input, Subtract, Concatenate, Dense, Conv2D
from keras.layers import Flatten, GlobalAveragePooling2D, MaxPooling2D, Dropout
from keras.applications.resnet50 import ResNet50
import tensorflow as tf

def test_si2019(problem_number=0, batch_size=16, seed=1337):
    """
    seed: Seed of random number used to split data for training and test
    """

    xshape = (224,224,3)

    # setup dataset instance used for training
    ds = SI2019Dataset(seed=seed)
    ds.set_problem_number(problem_number)
    num_classes = ds.num_classes()

    # design my model used for training
    tac_img = Input(shape=xshape)
    with tf.name_scope('ResNet50'):
        resnet = ResNet50(include_top=False, input_tensor=tac_img, weights='imagenet')
    x = GlobalAveragePooling2D()(resnet.output)
    x = Dense(1024, activation='relu')(x)
    success_rate = Dense(num_classes, activation='softmax')(x)
    model = keras.models.Model(inputs=[tac_img], outputs=[success_rate], name='screw')

    # train the model for N-class classification problem
    trainer.train_for_classification(ds,
                                         model,
                                         batch_size=batch_size,
                                         model_file_name='model.prob%d.hdf5' % problem_number)


import sys
import optparse

if __name__ == '__main__':
    parser = optparse.OptionParser()
    parser.add_option('-p', '--problem', dest='problem_number', type='int', help='[0,3)')
    options, args = parser.parse_args(sys.argv[1:])

    problem_number = options.problem_number
    if problem_number >= 0 and problem_number < 5:
        print('problem = ', problem_number)
        test_si2019(problem_number)
