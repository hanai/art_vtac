#!/usr/bin/env python3

from screw_direction import *
import numpy as np

import keras
import keras.backend as K
from keras.models import Sequential
from keras.layers import Input, Subtract, Concatenate, Dense, Conv2D
from keras.layers import Flatten, GlobalAveragePooling2D, MaxPooling2D, Dropout
from keras.applications.resnet50 import ResNet50
import tensorflow as tf

def test_screw_direction1(problem_number, batch_size=16, seed=1337):
    """
    Predict screw direction (up or down) from an image
    """
    xshape = (224,224,3)
    train_objects,test_objects = problem(problem_number)

    tac_during = Input(shape=xshape)
    with tf.name_scope('ResNet50'):
        resnet = ResNet50(include_top=False, input_tensor=tac_during, weights='imagenet')
    x = GlobalAveragePooling2D()(resnet.output)
    x = Dense(1024, activation='relu')(x)
    success_rate = Dense(1, activation='sigmoid')(x)
    model = keras.models.Model(inputs=[tac_during], outputs=[success_rate], name='screw')

    screw_direction(model, xshape,
                        random_train_test_split=False,
                        train_objects=train_objects,
                        test_objects=test_objects ,
                        batch_size=batch_size, seed=seed)



import sys
import optparse

if __name__ == '__main__':
    parser = optparse.OptionParser()
    parser.add_option('-p', '--problem', dest='problem_number', type='int', help='[0,3)')
    options, args = parser.parse_args(sys.argv[1:])

    if options.problem_number == None:
        print('problem number is required!')
    else:
        test_screw_direction1(options.problem_number)
