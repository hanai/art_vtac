#!/usr/bin/env python3

__author__ = "Ryo Hanai <ryo.hanai@aist.go.jp>"
__status__ = ""
__version__ = ""
__date__ = ""

import numpy as np
import time, math, datetime

import keras
from keras.models import load_model
from keras.optimizers import Adam
from keras.callbacks import TensorBoard, ModelCheckpoint

from vtac_common import dataset
import vtac_data
import re

class SI2019Dataset(dataset.Dataset):

    def __init__(self, seed=1337):
        self.dataset_key='190917'
        self._num_classes = 5
        self.scan_files()
        self.random_train_test_split(k=5, seed=seed)
        self.set_problem_number(0)

    def get_ids_for_generation(self):
        return self.train_ids()

    def input_tags(self):
        return ['image']

    def output_tags(self):
        return ['label']

    def data_shape(self, tag):
        return self._data[0][tag].shape

    def input_shape(self):
        return self.data_shape('image')

    def load(self, data_id):
        return self._data[self._table[data_id]]

    def num_classes(self):
        return self._num_classes

    def validation_data(self):
        itag = self.input_tags()[0]
        otag = self.output_tags()[0]
        xs = []
        ys = []
        for did in self.test_ids():
            d = self.load(did)
            xs.append(d[itag])
            ys.append(d[otag])
        return xs,ys

    def scan_files(self):
        self._table = {}
        self._data = vtac_data.load_data(self.dataset_key)
        otag = self.output_tags()[0]
        for i,d in enumerate(self._data):
            self._table[d['id']] = i
            d[otag] = keras.utils.to_categorical(d[otag], self._num_classes)

    def set_problem_number(self, number):
        """
        Data are split into k sub-groups for k-split cross validation.
        4 of them are used for training, and the rest is used for test.
        The problem number specifies which sub-group is used for test.
        """

        self._problem_number = number
        self._train_ids = np.concatenate(self._subgroup_ids[:number]+self._subgroup_ids[number+1:])
        self._test_ids = self._subgroup_ids[number]

        # add IDs of mirrored images for training data.
        mirrored_ids = []
        for did in self._train_ids:
            mirrored_ids.append(did+'m')
        self._train_ids = np.concatenate([self._train_ids, mirrored_ids])

        print('train ids: ', self.train_ids())
        print('test ids: ', self.test_ids())

    def random_train_test_split(self, k, seed):
        if seed != None: # fix the seed of random number
            np.random.seed(seed)

        # r_ids = []
        # l_ids = []
        # o_ids = []
        # for d in self._data:
        #     y = d['label']
        #     if y == 0:
        #         r_ids.append(d['id'])
        #     elif y == 1:
        #         l_ids.append(d['id'])
        #     else:
        #         o_ids.append(d['id'])

        # print('r ids:', r_ids) # 138 images
        # print('l ids:', l_ids) # 138 images
        # print('o ids:', o_ids) # 200 images

        ids = [d['id'] for d in self._data if not re.match('(17e_.*)|(.*m$)', d['id'])]
        self._subgroup_ids = []
        l = len(ids)
        shuffled_ids = np.random.permutation(l)
        for i in range(0,k):
            s = l * i // k
            e = l * (i+1) // k
            self._subgroup_ids.append(np.array(list(ids))[shuffled_ids[s:e]])
            print("SUBGROUP[%d]:"%i, self._subgroup_ids[i])

    def train_ids(self):
        return self._train_ids

    def test_ids(self, class_number=None):
        """
        class_number: 0,...,4
        """
        if class_number == None:
            return self._test_ids
        else:
            return [did for did in self._test_ids if np.argmax(self._data[self._table[did]]['label']) == class_number]

    def extra_ids(self):
        return [d['id'] for d in self._data if re.match('17e_.*', d['id'])]


from vtac_common.visualization import *
from keras.models import load_model

# excerpted samples for the figure in paper
r1_data_ids = ['09_5m', '09_44', '09_7m', '09_17']
l1_data_ids = ['09_34m', '09_14', '09_41m', '09_47m']
r2_data_ids = ['17_81m', '17_20', '17_17', '17_95m'] # '17_48', '17_95m'
l2_data_ids = ['17_29m', '17_46m', '17_15m', '17_86'] # '17_68'
o_data_ids = ['17_145m', '17_179', '17_113m', '17_182'] # '17_129m'

np.set_printoptions(precision=3, suppress=True)

class Validator:
    """
    Usage:
    v = Validator()
    v.set_problem(0) # problem number ranges over 0-4 (model file is loaded)
    result = v.predict() # validation using the subset not used for training
    v.show_result(result)

    result = v.predict(class_number=0) # use data in class 0 (one screw pointing right)
    result = v.predict(use_extra_data=True) # use extra data for validation
    """

    def __init__(self, seed=1337):
        self.ds = SI2019Dataset(seed=seed)

    def set_problem(self, problem_number):
        self.prob_num = problem_number
        self.model = load_model('model.prob%d.hdf5' % problem_number)

    def predict_on_ids(self, data_ids, return_one_hot=True):
        result = [self.predict1(did, return_one_hot) for did in data_ids]
        return result

    def predict1(self, did, return_one_hot=True):
        xshape = (224,224,3)
        rescale=1./255

        d = self.ds.load(did)
        img = d['image']
        lbl = d['label']
        # crop the center part of the image
        height,width,_ = img.shape
        dx,dy,_ = xshape
        x = int((width - dx)/2)
        y = int((height - dy)/2)
        # x = np.random.randint(0, width - dx + 1)
        # y = np.random.randint(0, height - dy + 1)
        cropped_img = img[y:(y+dy), x:(x+dx), :]
        # rescale
        cropped_img = cropped_img * rescale
        res = self.model.predict(cropped_img[np.newaxis, :, :])
        if not return_one_hot:
            res = np.argmax(res)
            lbl = np.argmax(lbl)
        return res,lbl,did

    def show_result(self, result):
        cols = 1
        dim = (len(result), cols)
        plt.figure(figsize=dim)

        def subplot(img, j):
            plt.subplot(dim[0], dim[1], j)
            plt.imshow(img)
            plt.axis('off')

        for i,d in enumerate(result):
            y,t,data_id = d
            e = self.ds.load(data_id)
            subplot(e['image'], cols*i+1)
            # plt.title(f'y={y:d}, t={t:d}, id={data_id}')
            plt.title(f'y={y:d}, t={t:d}')

        plt.tight_layout()
        plt.subplots_adjust(top=0.96, bottom=0.02, wspace=0.3, hspace=0.3)
        plt.show()

    def predict(self, class_number=None, use_extra_data=False):
        if use_extra_data:
            result = self.predict_on_ids(self.ds.extra_ids())
        else:
            result = self.predict_on_ids(self.ds.test_ids(class_number))
        return result
