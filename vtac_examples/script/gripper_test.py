#!/usr/bin/env python

import sys
import rospy
import actionlib
#import std_msgs.msg
import robotiq_msgs.msg

def send(client, position=0.0, timeout=3.0):
    goal = robotiq_msgs.msg.CModelCommandGoal()
    goal.force = 5.0
    goal.velocity = 0.1
    goal.position = position
    try:
        print("SEND GOAL")
        client.send_goal(goal)
        print("WAIT FOR RESULT")
        client.wait_for_result(rospy.Duration(timeout))
        result = client.get_result()
        print("RESULT=", result)
    except rospy.ROSInterruptException:
        rospy.loginfo("INTERRUPTED", file=sys.stderr)

# def gripper_test(n):
#     for i in range(n):
#         t1 = rospy.Time.now()
#         r.closeg()
#         t2 = rospy.Time.now()
#         print('*CLOSE TIME*= ', t2.to_sec()-t1.to_sec())
#         t3 = rospy.Time.now()
#         r.openg()
#         t4 = rospy.Time.now()
#         print('*OPEN TIME*= ', t4.to_sec()-t3.to_sec())

if __name__ == '__main__':
    if len(sys.argv) < 3:
        print("gripper_test.py {rhand|lhand} 0.02") # width in [m]
    else:
        rospy.init_node('gripper_test')
        if sys.argv[1] == 'rhand':
            topic = '/gripper/gripper_action_controller'
        elif sys.argv[1] == 'lhand':
            topic = '/gripper/gripper_action_controller'
        else:
            print('unknown gripper')

        client = actionlib.SimpleActionClient(topic, robotiq_msgs.msg.CModelCommandAction)
        client.wait_for_server()
        send(client, float(sys.argv[2]))
