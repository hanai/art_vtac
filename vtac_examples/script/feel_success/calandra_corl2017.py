# -*- coding: utf-8 -*-

'''
Loader and visualizer for visuo-tactile grasp learning using GelSight

# Reference:
- https://sites.google.com/view/the-feeling-of-success/
- [The Feeling of Success: Does Touch Sensing Help Predict Grasp Outcomes?](http://proceedings.mlr.press/v78/calandra17a/calandra17a.pdf)
'''

__author__ = "Ryo Hanai <ryo.hanai@aist.go.jp>"
__status__ = ""
__version__ = ""
__date__ = ""

import numpy as np
import matplotlib.pyplot as plt
import deepdish as dd
# import progressbar

from keras.utils import get_file

def load_data(path='calandra_corl2017_036.h5'):
    """
    Data files must be placed in .keras/datasets/ by downloading manually.
    Network downloading does not work, since for a large file, 
    google drive redirects to a page asking me whether I want to 
    proceed with the download without a virus scan.
    """
    path = get_file(path, origin = 'https://drive.google.com/uc?export=download&id=1fhpR8376Htmrcg3VYFe7mpORrYCqJ_OV')

    print('Loading file: %s' % path)
    t = dd.io.load(path)
    n_data = len(t)
    print("N data: %d" % n_data)
    return t

def visualize(data, idx):
    """
    Visualize data loaded by 'load_data()'.

    Arguments:
      data:
      idx: data index
    """

    experiment = data[idx]

    # fields = [
    #     'object_name',
    #     'is_gripping',
    #     'kinectA_rgb_before',
    #     'kinectA_rgb_during',
    #     'kinectA_rgb_after',
    #     'gelsightA_before',
    #     'gelsightA_during',
    #     'gelsightA_after',
    #     'gelsightB_before',
    #     'gelsightB_during',
    #     'gelsightB_after',
    # ]

    print('Object: %s' % t[idx]['object_name'])

    if experiment['is_gripping']:
        print('Grasp successful!')
    else:
        print('Grasp failed :(')

    # Kinect A
    # Before moving the gripper to the object
    plt.figure()
    plt.imshow(experiment['kinectA_rgb_before'])
    # Just before lift off (after closing the fingers)
    plt.figure()
    plt.imshow(experiment['kinectA_rgb_during'])
    # This is 4 seconds after attempting the lift
    plt.figure()
    plt.imshow(experiment['kinectA_rgb_after'])

    # Gelsight A (Rotated)
    # Before moving the gripper to the object
    plt.figure()
    plt.imshow(experiment['gelsightA_before'])
    # Just before lift off (after closing the fingers)
    plt.figure()
    plt.imshow(experiment['gelsightA_during'])
    # This is 4 seconds after attempting the lift
    plt.figure()
    plt.imshow(experiment['gelsightA_after'])

    # Gelsight B (Rotated)
    # Before moving the gripper to the object
    plt.figure()
    plt.imshow(experiment['gelsightB_before'])
    # Just before lift off (after closing the fingers)
    plt.figure()
    plt.imshow(experiment['gelsightB_during'])
    # This is 4 seconds after attempting the lift
    plt.figure()
    plt.imshow(experiment['gelsightB_after'])

    plt.show()


if __name__ == '__main__':
    t = load_data()
    visualize(t, idx=1)
