#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import numpy as np
import math
import threading
#import matplotlib.pyplot as plt
import calandra_corl2017

from keras.utils import get_file
from keras.preprocessing.image import ImageDataGenerator

# Pillow (image processing library)
from PIL import Image

# Marshaling with random access
# Pickling is not compatible between Python2 and 3.
# So, use deepdish instead.
import deepdish as dd
from deepdish.io import ls


class Preprocessor:
    """
    pp = Preprocessor()
    pp.process_files()

    """

    def __init__(self, y_boundary=100, out_image_format='ndarray', debug=False):
        self.y_boundary = y_boundary
        self.out_image_format = out_image_format
        self.debug = debug
        self.pattern = 'calandra_corl2017_%03d%s.h5'

    def data_file_name(self, file_number, suffix=''):
        if file_number < 0 or file_number > 37:
            raise Exception('file number must be in [0,37]')

        return self.pattern % (file_number, suffix)

    def process_rgb(self, img, return_ndarray=True):
        """
        Arguments:
        img : np.ndarray
        Returns:
        proprocessed image : np.ndarray
        """
        if img.shape != (293, 807, 3):
            raise Exception('unexpected image shape!')

        # convert to PIL Image
        pimg = Image.fromarray(img)

        # 1. CROP the image
        cropped_img = pimg.crop((0, self.y_boundary, pimg.size[0], pimg.size[1]))

        # 2. PAD vertically
        dim = cropped_img.size[0]
        pad_color = (0,0,0)
        padded_img = Image.new(cropped_img.mode, (dim, dim), pad_color)
        left = 0
        top = (cropped_img.size[0]-cropped_img.size[1]) // 2
        padded_img.paste(cropped_img, (left, top))

        # 3. RESIZE to (256, 256, 3)
        resized_img = padded_img.resize((256,256))

        if self.debug:
            show_image(cropped_img, 'cropped_rgb')
            show_image(padded_img, 'padded_rgb')
            show_image(resized_img, 'resized_rgb')

        if self.out_image_format == 'ndarray':
            resized_img = np.asarray(resized_img)
            # change the array mutable
            # resized_img.flags.writeable = True

        return resized_img

    def process_tactile(self, img):
        """
        Is is OK to just rescale the image?
        or, should I first crop the square image and rescale?
        """
        if img.shape != (960, 1280, 3):
            raise Exception('unexpected image shape!')

        pimg = Image.fromarray(img)
        resized_img = pimg.resize((256,256))

        if self.debug:
            show_image(resized_img, 'resized_tactile')

        if self.out_image_format == 'ndarray':
            resized_img = np.asarray(resized_img)
            # change the array mutable
            # resized_img.flags.writeable = True

        return resized_img

    def process(self, data, out_file=None, out_after=True):
        def update_images(d):
            d['kinectA_rgb_before'] = self.process_rgb(d['kinectA_rgb_before'])
            d['kinectA_rgb_during'] = self.process_rgb(d['kinectA_rgb_during'])
            if out_after:
                d['kinectA_rgb_after'] = self.process_rgb(d['kinectA_rgb_after'])
            else:
                del d['kinectA_rgb_after']
            d['gelsightA_before'] = self.process_tactile(d['gelsightA_before'])
            d['gelsightA_during'] = self.process_tactile(d['gelsightA_during'])
            if out_after:
                d['gelsightA_after'] = self.process_tactile(d['gelsightA_after'])
            else:
                del d['gelsightA_after']
            d['gelsightB_before'] = self.process_tactile(d['gelsightB_before'])
            d['gelsightB_during'] = self.process_tactile(d['gelsightB_during'])
            if out_after:
                d['gelsightB_after'] = self.process_tactile(d['gelsightB_after'])
            else:
                del d['gelsightB_after']

        for d in data:
            update_images(d)

        # save updated 'data' with deepdish
        if out_file != None:
            dd.io.save(out_file, data)

        return data

    def process_files(self, file_numbers=range(38), out_after=True):
        for n in file_numbers:
            data = calandra_corl2017.load_data(self.data_file_name(n, ''))
            self.process(data, out_file = self.data_file_name(n, '_2'), out_after=out_after)
