from preprocess import *
import dataset

import numpy as np
import time
import datetime

import keras
# import keras.backend as K
# from keras.layers import Input, Subtract, Concatenate, Dense, Conv2D
# from keras.layers import Flatten, GlobalAveragePooling2D, MaxPooling2D, Dropout
# from keras.applications.resnet50 import ResNet50
from keras.optimizers import Adam
from keras.callbacks import TensorBoard, ModelCheckpoint
# import tensorflow as tf

class GelSightDataset(dataset.Dataset):
    """
    This class load the data in a lazy manner, since the GelSight data size is too large
    to load in memory. Thus, scan_file() just scan the IDs in HDF files.
    """

    tags = ['kinectA_rgb_before',
            'kinectA_rgb_during',
            'gelsightA_before',
            'gelsightA_during',
            'gelsightB_before',
            'gelsightB_during']

    def __init__(self, file_numbers,
                     random_train_test_split=True,
                     train_objects=[],
                     test_objects=[],
                     seed=1337):
        self.lock = threading.Lock()
        self.file_pattern = 'calandra_corl2017_%03d_2.h5'
        self.scan_files(file_numbers)
        data_ids = list(self.table.values())[0]

        e = self.load(data_ids[0])
        self._shapes = {}
        for tag in self.tags:
            self._shapes[tag] = e[tag].shape
        self.__debug = False

        if random_train_test_split:
            if len(train_objects) > 0 or len(test_objects) > 0:
                print('object names are not specified when random_train_test_split is True')
            else:
                self.random_train_test_split(seed=seed)
        else:
            self._train_objects = train_objects
            self._test_objects = test_objects
            self.train_test_split()

    def scan_file(self, file_number):
        fpath = get_file(self.file_pattern % file_number, origin='')
        print('scanning ' + fpath)
        tree = ls.get_tree(fpath)
        data = tree.children['data']
        dlist = data.children
        for (data_number, e) in enumerate(dlist):
            name = e.children['object_name'].value.decode()
            is_gripping = e.children['is_gripping'].value
            try:
                self.table[name].append((file_number, data_number))
            except:
                self.table[name] = [(file_number, data_number)]
            self.inv_table[(file_number, data_number)] = name

    def scan_files(self, file_numbers):
        self.table = {}
        self.inv_table = {}
        for n in file_numbers:
            self.scan_file(n)

    def get_object_name_by_id(self, data_id):
        return self.inv_table[data_id]

    def index_list_by_object_names(self, object_names):
        l = []
        for name in object_names:
            try:
                l.extend(self.table[name])
            except:
                print('key %s not found' % name)
        return l

    def load(self, data_id):
        """
        dd.io.load() does not seem to be thread/process safe.
        When fit_generator() is called with two generators (one for train data, and
        the other for test data), Python interpreter fails with SEGV.
        It is not evaluated how much this lock degrades the performace of batch generation.
        """
        file_number, data_number = data_id
        fpath = get_file(self.file_pattern % file_number, origin='')
        with self.lock:
            # print('Load: %s, /data/i%d' % (fpath, data_number))
            e = dd.io.load(fpath, '/data/i%d' % data_number)
        return e

    def random_train_test_split(self, seed):
        if seed != None:
            np.random.seed(seed)

        l = len(self._data)
        shuffled_ids = np.random.permutation(l)
        m = math.ceil(l / 2)
        self._train_ids = shuffled_ids[:m]
        self._test_ids = shuffled_ids[m:]

    def train_test_split(self, seed):
        if seed != None:
            np.random.seed(seed)

        l = len(self.objects())
        shuffled_names = np.array(list(self.table.keys()))[np.random.permutation(l)]
        m = math.ceil(l / 2)
        self._train_objects = shuffled_names[:m]
        self._test_objects = shuffled_names[m:]

        print('train objects: ', self._train_objects)
        print('test objects: ', self._test_objects)

        self._train_ids = []
        self._test_ids = []
        for o in self._train_objects:
            self._train_ids.extend(self._table[o])
        for o in self._test_objects:
            self._test_ids.extend(self._table[o])

    def train_ids(self):
        return self._train_ids

    def test_ids(self):
        return self._test_ids

    def index_list_by_object_names(self, object_names):
        l = []
        for name in object_names:
            try:
                l.extend(self.table[name])
            except:
                print('key %s not found' % name)
        return l

    def show_statistics(self):
        def show_statistics_aux(object_names):
            n_total_successes = 0
            n_total_failures = 0
            for n in object_names:
                n_trials = 0
                n_successes = 0
                n_failures = 0
                dids = self.index_list_by_object_names([n])
                for did in dids:
                    e = self.load(did)
                    n_trials += 1
                    if e['is_gripping'] == True:
                        n_successes += 1
                    else:
                        n_failures += 1
                n_total_successes += n_successes
                n_total_failures += n_failures
                print(f'{n}, trials={n_trials}, succeses={n_successes}, failures={n_failures}, {dids}')
            print(f'Total: trials={n_total_successes+n_total_failures}, successes={n_total_successes}, failures={n_total_failures}')

        print('TRAIN OBJECTS:')
        show_statistics_aux(self._train_objects)
        print('TEST OBJECTS:')
        show_statistics_aux(self._test_objects)

    def objects(self):
        return self.table.keys() # sort alphabetically?

    def train_objects(self):
        return self._train_objects

    def test_objects(self):
        return self._test_objects


def feel_of_success(model, xshape, tags, batch_size=16, file_numbers=range(38),
                    n_validation_batches=100, n_epochs=50, rescale=1./255, seed=1337,
                    lr=1e-5):
    prefix = model.name

    def logdir(prefix, batch_size, seed, rescale):
        now_str = datetime.datetime.now().strftime('%y%m%d%H%M%S')
        r_suffix = 'n' if rescale == None else 'r'
        return f'./log/{prefix}-bs{batch_size:02d}-sd{seed:05d}-{r_suffix}-{now_str}'

    ds = GelSightDataset(file_numbers, seed=seed)
    vtg = FeelSuccessGenerator(horizontal_flip=True, random_crop=list(xshape[:2]), rescale=rescale)
    train_gen = vtg.flow_from_HDFs(ds, ds.train_objects(), batch_size=batch_size, tags=tags)
    test_gen = vtg.flow_from_HDFs(ds, ds.test_objects(), batch_size=batch_size, tags=tags)
    model.compile(loss='binary_crossentropy', optimizer=Adam(lr=lr), metrics=['binary_accuracy'])
    model.summary()
    tb_cb = TensorBoard(log_dir=logdir(prefix, batch_size, seed, rescale),
                            #histogram_freq=1,
                            write_graph=True,
                            #write_grads=True,
                            write_images=1,
                            #embeddings_freq=1
                            )
    md_cb = ModelCheckpoint("model.hdf5",
                            monitor='val_loss',
                            verbose=1,
                            save_best_only=True,
                            save_weights_only=False)

    # get several batches and merge them
    batches = []
    for i in range(n_validation_batches):
        batches.append(next(test_gen))

    xs,ys = zip(*batches)
    batch_merged = (list(map(np.concatenate, zip(*xs))), np.concatenate(ys))
    # show_batch(batch_merged[0][0], title='validation data', dim=(12,12))

    model.fit_generator(train_gen, steps_per_epoch=train_gen.N/20//batch_size,
        # validation_data=test_gen, validation_steps=test_gen.N/10//batch_size,
        validation_data=batch_merged,
        epochs=n_epochs, callbacks=[tb_cb, md_cb])

