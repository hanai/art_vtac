#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from feel_success import *
import numpy as np

import keras
import keras.backend as K
from keras.models import Sequential
from keras.layers import Input, Subtract, Concatenate, Dense, Conv2D
from keras.layers import Flatten, GlobalAveragePooling2D, MaxPooling2D, Dropout
from keras.applications.resnet50 import ResNet50
#from keras.utils import plot_model
import tensorflow as tf

def test_scan_data():
    """
    Scan calandra_corl2017_%03d_2*.h5 files in ~/.keras/datasets,
    and return object IDs belonging to the specified objects.
    Object ID is a pair of a file number and a data number in the file,
    """
    train_object_names = ['soft_beer_bottle_holder',
                          'rubics_cube',
                          'pig',
                          'soft_zebra',
                          'aspirin',
                          'peanut_butter']

    test_object_names = ['soft_blue_hexagon',
                         'stuffed_beachball',
                         'soda_can',
                         'brown_paper_cup_2_upside',
                         'peptobismol']

    ds = GelSightDataset(file_numbers=range(5))
    train_indices = ds.index_list_by_object_names(train_object_names)
    test_indices = ds.index_list_by_object_names(test_object_names)
    return train_indices, test_indices

def test_generator(file_numbers=range(5)):
    ds = GelSightDataset(file_numbers)
    vtg = VisTacGenerator(zca_whitening=True, horizontal_flip=True, random_crop=[224,224])
    object_names = ds.objects()
    gen = vtg.flow_from_HDFs(ds, object_names)
    x,_ = next(gen)
    for i, title in enumerate(GelSightDataset.tags):
        show_batch(x[i], title=title)
    show_batch_subimg(x[2], x[3], title='gelsightA_sub')
    show_batch_subimg(x[4], x[5], title='gelsightB_sub')
    return gen

class PredictionTester:
    """
    pt = PredictionTester(batch_size=8)
    pt.test(false_positive_only=False)

    pt = PredictionTester(batch_size=64)
    pt.test()
    """

    def __init__(self, modelfile='model.hdf5', file_numbers=range(38),
                     batch_size=32, seed=1337, rescale=1./255,
                     tags=['gelsightA_before', 'gelsightA_during']):
        xshape = (224,224,3)
        self.model = load_model(modelfile)
        self.ds = GelSightDataset(file_numbers, seed=seed)
        vtg = VisTacGenerator(horizontal_flip=True, random_crop=list(xshape[:2]), rescale=rescale)
        self.gen = vtg.flow_from_HDFs(self.ds, self.ds.test_objects(), batch_size=batch_size, tags=tags)

    def test(self, false_positive_only=True, threshold=0.9):
        x,y,data_id = self.gen.__next__(with_data_id=True)
        t = self.model.predict(x)
        # np.concatenate((t,y), axis=1)
        self.result = list(zip(t,y,data_id))
        self.show_result(self.result, false_positive_only=false_positive_only, threshold=threshold)

    def false_positive(self, result, threshold):
        def is_false_positive(d, eps=1e-3):
            t,y,_ = d
            return t > threshold and y < eps
        return [x for x in result if is_false_positive(x)]

    def show_result(self, result, false_positive_only, threshold):
        if false_positive_only:
            result = self.false_positive(result, threshold)

        cols = 5
        dim = (len(result), cols)
        plt.figure(figsize=dim)

        def subplot(img, j):
            plt.subplot(dim[0], dim[1], j)
            plt.imshow(img)
            plt.axis('off')

        def sub(img1, img2):
            return np.array(np.floor((img1/2 - img2/2)) + 128, dtype=np.uint8)

        for i,d in enumerate(result):
            t,y,data_id = d
            e = self.ds.load(data_id)
            subplot(e['kinectA_rgb_during'], cols*i+1)
            subplot(e['gelsightA_during'], cols*i+2)
            subplot(e['gelsightB_during'], cols*i+3)
            subplot(sub(e['gelsightA_before'], e['gelsightA_during']), cols*i+4)
            subplot(sub(e['gelsightB_before'], e['gelsightB_during']), cols*i+5)
            plt.title(f't={t[0]:f}, y={y[0]}, did={data_id}, {self.ds.get_object_name_by_id(data_id)}')

        plt.tight_layout()
        plt.subplots_adjust(top=0.92, wspace=0.2, hspace=0.2)
        plt.show()

def test_generation_time():
    ds = GelSightDataset(file_numbers=range(38))
    vtg = VisTacGenerator(ds, horizontal_flip=True, random_crop=[224,224])
    object_names = ds.object_names()
    gen = vtg.flow_from_HDFs(ds, object_names)
    for _ in range(10):
        start_time = time.clock()
        gen.__next__()
        end_time = time.clock()
        print(end_time - start_time)

def test_tactile_CNN(batch_size=16, file_numbers=range(38), seed=1337):
    """
    Predict grasp success from single Tactile image using a simple CNN.
    """
    xshape = (224,224,3)
    tags = ['gelsightA_during']

    model = Sequential(name='tac_furing_cnn')
    model.add(Conv2D(32, (3,3), activation='relu', input_shape=xshape))
    model.add(Conv2D(64, (3,3), activation='relu'))
    model.add(MaxPooling2D(pool_size=(2,2)))
    model.add(Conv2D(128, (3,3), activation='relu'))
    model.add(MaxPooling2D(pool_size=(2,2)))
    model.add(Flatten())
    model.add(Dense(256, activation='relu'))
    model.add(Dropout(0.4))
    model.add(Dense(1, activation='sigmoid'))

    feel_of_success(model, xshape, tags=tags, batch_size=batch_size, file_numbers=file_numbers, seed=seed)

def test_tactile_during(batch_size=16, file_numbers=range(38), seed=1337):
    """
    Predict grasp success from single Tactile image
    """
    xshape = (224,224,3)
    tags = ['gelsightA_during']

    tac_during = Input(shape=xshape)
    with tf.name_scope('ResNet50'):
        resnet = ResNet50(include_top=False, input_tensor=tac_during, weights='imagenet')
    x = GlobalAveragePooling2D()(resnet.output)
    x = Dense(1024, activation='relu')(x)
    success_rate = Dense(1, activation='sigmoid')(x)
    model = keras.models.Model(inputs=[tac_during], outputs=[success_rate], name='tac_during')

    feel_of_success(model, xshape, tags=tags, batch_size=batch_size,
                    file_numbers=file_numbers, seed=seed)

def test_rgb_during(batch_size=16, file_numbers=range(38), seed=1337):
    """
    Predict grasp success from single RGB image
    """
    xshape = (224,224,3)
    tags = ['kinectA_rgb_during']

    rgb_during = Input(shape=xshape)
    with tf.name_scope('ResNet50'):
        resnet = ResNet50(include_top=False, input_tensor=rgb_during, weights='imagenet')
    x = GlobalAveragePooling2D()(resnet.output)
    x = Dense(1024, activation='relu')(x)
    success_rate = Dense(1, activation='sigmoid')(x)
    model = keras.models.Model(inputs=[rgb_during], outputs=[success_rate], name='rgb_during')

    feel_of_success(model, xshape, tags=tags, batch_size=batch_size,
                    file_numbers=file_numbers, seed=seed)
    
def test_tactile_sub(batch_size=16, file_numbers=range(38), seed=1337):
    """
    Predict grasp success from the difference between TWO tactile images: BEFORE and DURING grasp
    """
    xshape = (224,224,3)
    tags = ['gelsightA_before', 'gelsightA_during']

    tac_before = Input(shape=xshape)
    tac_during = Input(shape=xshape)
    subtracted = Subtract()([tac_before, tac_during])
    with tf.name_scope('ResNet50'):
        resnet = ResNet50(include_top=False, input_tensor=subtracted, weights='imagenet')
    x = GlobalAveragePooling2D()(resnet.output)
    x = Dense(1024, activation='relu')(x)
    success_rate = Dense(1, activation='sigmoid')(x)
    model = keras.models.Model(inputs=[tac_before, tac_during], outputs=[success_rate], name='tac_sub')

    feel_of_success(model, xshape, tags=tags, batch_size=batch_size,
                    file_numbers=file_numbers, seed=seed)

def test_2tactiles(batch_size=16, file_numbers=range(38), seed=1337):
    """
    Predict grasp success from the two subtracted images obtained by two tactile sensors.
    """
    xshape = (224,224,3)
    tags = ['gelsightA_before', 'gelsightA_during', 'gelsightB_before', 'gelsightB_during']

    tacA_before = Input(shape=xshape)
    tacA_during = Input(shape=xshape)
    tacB_before = Input(shape=xshape)
    tacB_during = Input(shape=xshape)
    subtractedA = Subtract()([tacA_before, tacA_during])
    subtractedB = Subtract()([tacB_before, tacB_during])
    with tf.name_scope('ResNet50'):
        resnet = ResNet50(include_top=False, weights='imagenet')

    A = resnet(subtractedA)
    B = resnet(subtractedB)
    channel_axis = 1 if K.image_data_format() == 'channels_first' else 3
    x = Concatenate(axis=channel_axis)([A, B])
    x = GlobalAveragePooling2D()(x)
    x = Dense(1024, activation='relu')(x)
    success_rate = Dense(1, activation='sigmoid')(x)
    model = keras.models.Model(inputs=[tacA_before, tacA_during, tacB_before, tacB_during],
                                outputs=[success_rate], name='two_tacs_sub')

    feel_of_success(model, xshape, tags=tags, batch_size=batch_size,
                    file_numbers=file_numbers, seed=seed)

def test_2tactiles_no_global_pooling(batch_size=16, file_numbers=range(38), seed=1337):
    """
    Predict grasp success from the two subtracted images obtained by two tactile sensors.
    The network uses Flatten instead of GlobalAveragePooling.
    """
    xshape = (224,224,3)
    tags = ['gelsightA_before', 'gelsightA_during', 'gelsightB_before', 'gelsightB_during']

    tacA_before = Input(shape=xshape)
    tacA_during = Input(shape=xshape)
    tacB_before = Input(shape=xshape)
    tacB_during = Input(shape=xshape)
    subtractedA = Subtract()([tacA_before, tacA_during])
    subtractedB = Subtract()([tacB_before, tacB_during])
    with tf.name_scope('ResNet50'):
        resnet = ResNet50(include_top=False, weights='imagenet')

    A = resnet(subtractedA)
    B = resnet(subtractedB)
    channel_axis = 1 if K.image_data_format() == 'channels_first' else 3
    x = Concatenate(axis=channel_axis)([A, B])
    x = Flatten()(x)
    x = Dense(20, activation='relu')(x)
    success_rate = Dense(1, activation='sigmoid')(x)
    model = keras.models.Model(inputs=[tacA_before, tacA_during, tacB_before, tacB_during],
                                outputs=[success_rate], name='two_tacs_sub')

    feel_of_success(model, xshape, tags=tags, batch_size=batch_size,
                    file_numbers=file_numbers, seed=seed)

def test_2tactiles_no_hidden_layer(batch_size=16, file_numbers=range(38), seed=1337):
    """
    Predict grasp success from the two subtracted images obtained by two tactile sensors.
    The network directly maps the output of GlobalAveragePooling to [0,1].
    """
    xshape = (224,224,3)
    tags = ['gelsightA_before', 'gelsightA_during', 'gelsightB_before', 'gelsightB_during']

    tacA_before = Input(shape=xshape)
    tacA_during = Input(shape=xshape)
    tacB_before = Input(shape=xshape)
    tacB_during = Input(shape=xshape)
    subtractedA = Subtract()([tacA_before, tacA_during])
    subtractedB = Subtract()([tacB_before, tacB_during])
    with tf.name_scope('ResNet50'):
        resnet = ResNet50(include_top=False, weights='imagenet')

    A = resnet(subtractedA)
    B = resnet(subtractedB)
    channel_axis = 1 if K.image_data_format() == 'channels_first' else 3
    x = Concatenate(axis=channel_axis)([A, B])
    x = GlobalAveragePooling2D()(x)
    success_rate = Dense(1, activation='sigmoid')(x)
    model = keras.models.Model(inputs=[tacA_before, tacA_during, tacB_before, tacB_during],
                                outputs=[success_rate], name='two_tacs_sub')

    feel_of_success(model, xshape, tags=tags, batch_size=batch_size,
                    file_numbers=file_numbers, seed=seed)

def test_vtac1(batch_size=16, file_numbers=range(38), seed=1337):
    xshape = (224,224,3)
    tags = ['kinectA_rgb_during', 'gelsightA_before', 'gelsightA_during']
    print('not yet implemented')


import sys
import optparse

if __name__ == '__main__':
    parser = optparse.OptionParser()
    parser.add_option('-t', '--test', dest='test_name', type='string', help='preprocess | generator | network')
    parser.add_option('-p', '--preprocess', dest='file_number', type='int', help='e.g. 37')
    parser.add_option('-a', '--preprocess-all', dest='preprocess_all',
                          action='store_true', default=False, help='')
    options, args = parser.parse_args(sys.argv[1:])

    # print(options.test_name)
    # print(options.file_number)
    # print(options.preprocess_all)

    if options.test_name != None:
        if options.test_name == 'preprocess':
            test_preprocess()
        elif options.test_name == 'generator':
            test_generator(range(4,38))
        elif options.test_name == 'generation_time':
            test_generation_time()
        elif options.test_name == 'network':
            test_network()
    elif options.file_number:
        preprocess_files(options.file_number)
    elif options.preprocess_all:
        preprocess_files(range(38))
