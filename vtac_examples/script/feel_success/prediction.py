from visualization import *
from keras.models import load_model

# ds = GelSightDataset(file_numbers, seed=seed)
# PredictionTester(ds)

class PredictionTester:
    """
    pt = PredictionTester(batch_size=8)
    pt.test(false_positive_only=False)

    pt = PredictionTester(batch_size=64)
    pt.test()
    """

    def __init__(self, dataset, modelfile='model.hdf5', file_numbers=range(38),
                     batch_size=32, seed=1337, rescale=1./255,
                     tags=['gelsightA_before', 'gelsightA_during']):
        xshape = (224,224,3)
        self.model = load_model(modelfile)
        self.ds = dataset

        #vtg = FeelSuccessGenerator(horizontal_flip=True, random_crop=list(xshape[:2]), rescale=rescale)
        vtg = FeelSuccessGenerator(horizontal_flip=True, random_crop=list(xshape[:2]), rescale=rescale,
                                zca_whitening=True)
        # vtg = FeelSuccessGenerator(horizontal_flip=True, random_crop=list(xshape[:2]), rescale=rescale,
        #                         rotation_range=20, zoom_range=0.2, brightness_range=[0.8,1.0])
        self.gen = vtg.flow_from_HDFs(self.ds, self.ds.test_objects(), batch_size=batch_size, tags=tags)

    def test(self, false_positive_only=True, threshold=0.9):
        x,y,data_id = self.gen.__next__(with_data_id=True)
        t = self.model.predict(x)
        # np.concatenate((t,y), axis=1)
        self.result = list(zip(t,y,data_id))
        self.show_result(self.result, false_positive_only=false_positive_only, threshold=threshold)

    def false_positive(self, result, threshold):
        def is_false_positive(d, eps=1e-3):
            t,y,_ = d
            return t > threshold and y < eps
        return [x for x in result if is_false_positive(x)]

    def show_result(self, result, false_positive_only, threshold):
        if false_positive_only:
            result = self.false_positive(result, threshold)

        cols = 5
        dim = (len(result), cols)
        plt.figure(figsize=dim)

        def subplot(img, j):
            plt.subplot(dim[0], dim[1], j)
            plt.imshow(img)
            plt.axis('off')

        def sub(img1, img2):
            return np.array(np.floor((img1/2 - img2/2)) + 128, dtype=np.uint8)

        for i,d in enumerate(result):
            y,t,data_id = d
            e = self.ds.load(data_id)
            subplot(e['kinectA_rgb_during'], cols*i+1)
            subplot(e['gelsightA_during'], cols*i+2)
            subplot(e['gelsightB_during'], cols*i+3)
            subplot(sub(e['gelsightA_before'], e['gelsightA_during']), cols*i+4)
            subplot(sub(e['gelsightB_before'], e['gelsightB_during']), cols*i+5)
            plt.title(f'y={t[0]:f}, t={y[0]}, did={data_id}, {self.ds.get_object_name_by_id(data_id)}')

        plt.tight_layout()
        plt.subplots_adjust(top=0.92, wspace=0.2, hspace=0.2)
        plt.show()
