#!/usr/bin/env python

import numpy as np
from rspt_dev.robot_if import *

def init():
    global r
    r = UR5eHande()
    r.set_base_frame(r.get_frame('workspace_center'))
    # r.getl()

def random_pick():
    home_pose = ([0.0,-0.02,0.3],[0,0,0,1])
    approach_pose = ([0.0,-0.02,0.15],[0,0,0,1])
    grasp_pose = ([0.0,-0.02,0.12],[0,0,0,1])

    r.movel(home_pose)
    for i in range(3):
        r.openg()
        r.movel(approach_pose)
        r.movel(grasp_pose)
        r.closeg()
        r.movel(approach_pose)
        # do recognition
        # do placing
        # r.movel(approach_pose)

# class PickMotionTask(Task):
#     def __init__(self, robot):
#         super(PickMotionTask, self).__init__(robot)
#         self.params = [
#             PickParam(0.1, 0.1, 0.5, 10),
#             PickParam(-0.1, 0.1, 0.2, 10),
#             PickParam(-0.1, -0.1, -0.5, 10),
#             PickParam(0.1, -0.1, 0.0, 10)
#             ]

#     def pick(self, param):
#         p0, q0 = self.r.get_frame('workspace_center')
#         p_goal = p0 + np.array([param.x, param.y, 0.035])
#         q_goal = t.quaternion_multiply(t.quaternion_from_euler(0, 0, param.theta), q0)
#         p_app = p_goal + np.array([0, 0, 0.03])
#         q_app = q_goal
#         self.r.set_frame('object1', p_goal, q_goal)

#         self.r.openg()
#         self.r.movel(p_app, q_app)
#         self.r.movel(p_goal, q_goal)
#         self.r.closeg()
#         self.r.movel(p_app, q_app)

#     def run(self):
#         for param in self.params:
#             self.pick(param)

# class MoveUSBKeyTask(Task):
#     def __init__(self, robot):
#         super(MoveUSBKeyTask, self).__init__(robot)

#     def run(self):
#         p0, q0 = self.r.get_frame('workspace_center')
#         p_obj = p0 + np.array([0, 0, 0.03])
#         q_obj = t.quaternion_multiply(t.quaternion_from_euler(0, 0, 0), q0)
#         p_obj0 = p_obj + np.array([0, 0, 0.06])
#         q_obj0 = q_obj
#         self.r.set_frame('from', p_obj, q_obj)
#         self.r.openg()
#         self.r.movel(p_obj0, q_obj0)
#         self.r.movel(p_obj, q_obj)
#         self.r.closeg()
#         self.r.movel(p_obj0, q_obj0)

#         p_goal = p0 + np.array([0, -0.028, 0.03])
#         q_goal = t.quaternion_multiply(t.quaternion_from_euler(0, 0, 0), q0)
#         p_goal0 = p_goal + np.array([0, 0, 0.06])
#         q_goal0 = q_goal
#         self.r.set_frame('to', p_goal, q_goal)
#         self.r.movel(p_goal0, q_goal0)
#         self.r.movel(p_goal, q_goal)
#         self.r.openg()
#         self.r.movel(p_goal0, q_goal0)

# class PPSomethingTask(Task):
#     def __init__(self, robot):
#         super(PPSomethingTask, self).__init__(robot)

#     def run(self):
#         p0, q0 = self.r.get_frame('workspace_center')
#         p_obj = p0 + np.array([0, 0, 0.015])
#         q_obj = t.quaternion_multiply(t.quaternion_from_euler(0, 0, 0), q0)
#         p_obj0 = p_obj + np.array([0, 0, 0.06])
#         q_obj0 = q_obj
#         self.r.set_frame('from', p_obj, q_obj)
#         self.r.openg()
#         self.r.movel(p_obj0, q_obj0)
#         self.r.movel(p_obj, q_obj)
#         self.r.closeg()
#         self.r.movel(p_obj0, q_obj0)

#         p_goal = p0 + np.array([0, -0.028, 0.015])
#         q_goal = t.quaternion_multiply(t.quaternion_from_euler(0, 0, 0), q0)
#         p_goal0 = p_goal + np.array([0, 0, 0.06])
#         q_goal0 = q_goal
#         self.r.set_frame('to', p_goal, q_goal)
#         self.r.movel(p_goal0, q_goal0)
#         self.r.movel(p_goal, q_goal)
#         self.r.openg()
#         self.r.movel(p_goal0, q_goal0)


if __name__ == '__main__':
    init()
