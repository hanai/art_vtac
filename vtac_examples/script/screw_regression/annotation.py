# -*- coding: utf-8 -*-

import os, sys, glob, re, time
import cv2
import numpy as np
import matplotlib.pyplot as plt
import copy

data_directory = os.path.join(os.environ['HOME'], 'Dataset/vtacdata190917-YOLO/dlabels')

def read_label(data_id='17_1'):
    """ bounding box ::= [cx, cy, w, h] """
    label_file = os.path.join(data_directory, 'labels', '%s.txt'%data_id)
    with open(label_file, 'r') as f:
        sbbs = [line.strip().split(' ')[1:] for line in f.readlines()]
        return [[float(s) for s in sbb] for sbb in sbbs]
    
def read_image(data_id='17_1'):
    image_file = os.path.join(data_directory, 'images', '%s.png'%data_id)
    image = plt.imread(image_file)
    return image

def load_regions(data_id='17_1'):
    bboxes = read_label(data_id)
    image = read_image(data_id)
    print(bboxes)

    regions = []
    for bbox in bboxes:
        cx, cy, w, h = bbox
        xmin = round(256 * (cx - w/2.0))
        ymin = round(256 * (cy - h/2.0))
        xmax = round(256 * (cx + w/2.0))
        ymax = round(256 * (cy + h/2.0))
        cropped_image = image[ymin:ymax, xmin:xmax]
        regions.append(cropped_image)
    return regions

def get_data_ids():
    file_list = glob.glob(os.path.join(data_directory, 'labels/*.txt'))
    return [re.sub('\.txt', '', re.sub('.*/labels/', '', f)) for f in file_list]


cv2.namedWindow(winname="win1")

#data_ids = get_data_ids()
data_ids = ['17_69']
print(data_ids)

start_point = None
end_flag = False

for data_id in data_ids:
    if end_flag:
        break

    print('Annotating %s.png' % data_id)
    d_label_file = os.path.join(data_directory, 'dlabels', '%s.txt'%data_id)

    with open(d_label_file, 'w') as df:
        regions = load_regions(data_id)
        for rid, region in enumerate(regions):
            print('Region %d' % rid)
            start_point = None
            dvector = None
            img = copy.copy(region)
            def mouse_handler(event, x, y, flags, param):
                global start_point, dvector
                if event == cv2.EVENT_LBUTTONDOWN:
                    if start_point == None:
                        start_point = (x,y)
                    else:
                        dvector = np.array([x,y]) - np.array(start_point)
                        print(dvector)
                        start_point = None
                elif event == cv2.EVENT_RBUTTONDOWN:
                    start_point = None
                elif event == cv2.EVENT_MOUSEMOVE:
                    if start_point != None:
                        img = copy.copy(region)
                        cv2.line(img, start_point, (x,y), (0,255,0), 3)
                        cv2.imshow("win1", img)

            cv2.setMouseCallback("win1", mouse_handler)
            cv2.imshow("win1", img)

            while True:
                key = cv2.waitKey(10)
                if key == 27 or key == 32: # Esc or Space
                    if type(dvector) == np.ndarray:
                        print('write a direction vector')
                        df.write('%d %d\n' % (dvector[0], dvector[1]))
                        dvector = None

                    if key == 27: # Esc
                        end_flag = True
                    break

cv2.destroyAllWindows()
        
