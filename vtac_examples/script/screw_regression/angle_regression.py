# -*- coding: utf-8 -*-

import os, sys, glob, re, time
import cv2
import numpy as np
from scipy.linalg import norm
import matplotlib.pyplot as plt
from annotation_util import *


import tensorflow as tf
from tensorflow import keras
from tensorflow.keras import layers, losses
from tensorflow.keras.models import Model

from keras.layers import Conv2D, MaxPooling2D, Dense, Input, UpSampling2D, BatchNormalization
from keras.callbacks import EarlyStopping, TensorBoard


def get_train_data_ids():
    data_directory = os.path.join(os.environ['HOME'], 'Program/datasets/vtacdata190917/vtacdata190917-YOLO/labels/train')
    return scan_data_ids(data_directory)

def get_val_data_ids():
    data_directory = os.path.join(os.environ['HOME'], 'Program/datasets/vtacdata190917/vtacdata190917-YOLO/labels/val')
    return scan_data_ids(data_directory)

def load_dlabels(data_id='17_1'):
    labels = []
    with open(os.path.join(data_directory, 'dlabels', '%s.txt'%data_id)) as f:
        for line in f.readlines():
            x,y = map(float, line.split())
            v = np.array([x,y])
            labels.append(v / norm(v))
    return labels

def process_input_image(image):
    h = image.shape[0]
    w = image.shape[1]
    a = np.zeros((256, 256, 3))
    a[128-int(h/2):128-int(h/2)+h, 128-int(w/2):128-int(w/2)+w, :3] = image[:,:,:3]
    return a
    
def load_dataset(data_ids):
    xs = []
    ts = []
    
    for data_id in data_ids:
        regions = load_regions(data_id)
        labels = load_dlabels(data_id)
        for region,label in zip(regions, labels):
            xs.append(process_input_image(region))
            ts.append(label)

    return np.array(xs), np.array(ts)

class Regressor(tf.keras.Model):

    def __init__(self):
        super(Regressor, self).__init__()

        self.model = tf.keras.Sequential([
            tf.keras.layers.InputLayer(input_shape=(256, 256, 3)),

            tf.keras.layers.Conv2D(8, kernel_size=3, strides=1, padding='same', activation='selu'),
            tf.keras.layers.BatchNormalization(),
            tf.keras.layers.MaxPool2D(pool_size=2),

            tf.keras.layers.Conv2D(16, kernel_size=3, strides=1, padding='same', activation='selu'),
            tf.keras.layers.BatchNormalization(),
            tf.keras.layers.MaxPool2D(pool_size=2),

            tf.keras.layers.Conv2D(32, kernel_size=3, strides=1, padding='same', activation='selu'),
            tf.keras.layers.BatchNormalization(),
            tf.keras.layers.MaxPool2D(pool_size=2),

            tf.keras.layers.Conv2D(64, kernel_size=3, strides=1, padding='same', activation='selu'),
            tf.keras.layers.BatchNormalization(),
            tf.keras.layers.MaxPool2D(pool_size=2),

            tf.keras.layers.Flatten(),
            tf.keras.layers.Dense(100, activation='selu'),
            tf.keras.layers.Dense(2, activation=None),
            
        ])

    def call(self, x):
        return self.model(x)


train_xs, train_ts = load_dataset(get_train_data_ids())
val_xs, val_ts = load_dataset(get_val_data_ids())

opt = keras.optimizers.Adamax(learning_rate=0.001)

model = Regressor()
model.compile(loss='mse', optimizer=opt)

# create checkpoint and save best weight
checkpoint_path = "/home/ryo/Program/Ashesh_colab/ae_cp/cp.ckpt"
checkpoint_dir = os.path.dirname(checkpoint_path)


def train():

    # see model summary
    model.model.summary()

    start = time.time()

    # Create a callback that saves the model's weights
    cp_callback = tf.keras.callbacks.ModelCheckpoint(filepath=checkpoint_path,
                                                     save_weights_only=True,
                                                     verbose=1,
                                                     mode='min',
                                                     save_best_only=True)

    # early stopping if not changing for 50 epochs
    early_stop = EarlyStopping(monitor='val_loss',
                               patience=100)

    # reduce learning rate
    reduce_lr = tf.keras.callbacks.ReduceLROnPlateau(monitor='val_loss', 
                                                     factor=0.1,
                                                     patience=50, 
                                                     verbose=1,
                                                     min_lr=0.00001)

    # train the model
    history = model.fit(train_xs,
                        train_ts, 
                        epochs=300,
                        batch_size=32,
                        shuffle=True,
                        validation_data=(val_xs, val_ts),
                        callbacks=[cp_callback, early_stop, reduce_lr])

    end = time.time()
    print('\ntotal time spent {}'.format((end-start)/60))

    plt.plot(history.epoch, history.history['loss'], label='train_loss')
    plt.plot(history.epoch, history.history['val_loss'], label='test_loss')
    plt.title('Epochs on Training Loss')
    plt.xlabel('# of Epochs')
    plt.ylabel('Mean Squared Error')
    plt.legend()
    plt.show()
    return history

def angle_of_vectors(v1, v2):
    return np.arccos(np.dot(v1, v2) / (norm(v1)*norm(v2))) * 180.0 / np.pi

def test():
    # load_best_checkpoint and evaluate
    cp_model = Regressor()
    cp_model.compile(loss='mse', optimizer=opt)
    cp_model.load_weights(checkpoint_path)
    cp_loss = cp_model.evaluate(val_xs, val_ts)

    # evaluate the model on the test set
    # final_loss = gaussian_auto_encoder.evaluate(gaussian_test_ds, test_ds)

    """# DENOISED IMAGES"""

    # run model on the test_ds to reconstruct
    cp_result = cp_model.predict(val_xs)
    # print('PREDICTED: ', cp_result)
    # print('LABEL: ', val_ts)
    degrees = list(map(lambda x: angle_of_vectors(x[0],x[1]), zip(cp_result, val_ts)))
    # for i, angle in enumerate(degrees):
    #     print(i, angle)

    return degrees
    # n = 10
    # #idx = [np.random.randint(1,20) for i in range(n)]
    # idx = [np.random.randint(1,20) for i in range(n)]
    # plt.figure(figsize=(20, 4))
    # for i in range(n):
    #     # display original
    #     ax = plt.subplot(3, n, i + 1)
    #     plt.title("original")
    #     plt.imshow(test_ds[idx[i]])
    #     ax.get_xaxis().set_visible(False)
    #     ax.get_yaxis().set_visible(False)

    #     # display reconstruction
    #     cx = plt.subplot(3, n, i + n + 1)
    #     plt.title("reconstructed")
    #     plt.imshow(cp_result[idx[i]])
    #     cx.get_xaxis().set_visible(False)
    #     cx.get_yaxis().set_visible(False)

    # plt.show()
