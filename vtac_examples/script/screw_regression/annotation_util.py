# -*- coding: utf-8 -*-

import os, sys, glob, re, time
import cv2
import numpy as np
import matplotlib.pyplot as plt

data_directory = os.path.join(os.environ['HOME'], 'Dataset/vtacdata190917-YOLO')

def read_label(data_id='17_1'):
    """ bounding box ::= [cx, cy, w, h] """
    label_file = os.path.join(data_directory, 'labels', '%s.txt'%data_id)
    with open(label_file, 'r') as f:
        sbbs = [line.strip().split(' ')[1:] for line in f.readlines()]
        return [[float(s) for s in sbb] for sbb in sbbs]
    
def read_image(data_id='17_1'):
    image_file = os.path.join(data_directory, 'images', '%s.png'%data_id)
    image = plt.imread(image_file)
    return image

def load_regions(data_id='17_1'):
    bboxes = read_label(data_id)
    image = read_image(data_id)
    print(bboxes)

    regions = []
    for bbox in bboxes:
        cx, cy, w, h = bbox
        xmin = round(256 * (cx - w/2.0))
        ymin = round(256 * (cy - h/2.0))
        xmax = round(256 * (cx + w/2.0))
        ymax = round(256 * (cy + h/2.0))
        cropped_image = image[ymin:ymax, xmin:xmax]
        regions.append(cropped_image)
    return regions

def scan_data_ids(data_directory):
    file_list = glob.glob(os.path.join(data_directory, '*.txt'))
    return [os.path.splitext(os.path.basename(f))[0] for f in file_list]

def get_data_ids():
    return scan_data_ids(os.path.join(data_directory, 'labels'))

