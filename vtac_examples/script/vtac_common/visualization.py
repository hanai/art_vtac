#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import numpy as np
import matplotlib.pyplot as plt

__author__ = "Ryo Hanai <ryo.hanai@aist.go.jp>"
__status__ = ""
__version__ = ""
__date__ = ""

def show_image(img, title='image'):
    if type(img) == np.ndarray:
        plt.figure()
        plt.title(title)
        cmap = None
        if img.dtype != 'uint8':
            img = img.astype('uint8')
        if len(img.shape) == 2:
            cmap = 'gray'
        plt.imshow(img, cmap=cmap)
        plt.show()
    elif type(img) == Image.Image:
        img.show(title=title) # Not all viewers support the title
    else:
        Exception('unknow image type')

def show_heatmap(img, title='heatmap'):
    if type(img) != np.ndarray:
        Exception('img must be of type numpy.ndarray')
        return
    plt.figure()
    plt.title(title)
    # if img.dtype != 'uint8':
    #     img = img.astype('uint8')
    plt.imshow(img, vmin=0, vmax=1, cmap='jet')
    # plg.colorbar()

def show_batch(imgs, title='', dim = (6,6)):
    plt.figure(figsize=dim)
    for i in range(imgs.shape[0]):
        img = imgs[i]
        plt.subplot(dim[0], dim[1], i+1)
        plt.imshow(img.astype('uint8'), interpolation='nearest')
        plt.axis('off')

    plt.suptitle(title)
    plt.tight_layout()
    plt.subplots_adjust(top=0.92, wspace=0.1, hspace=0.1)
    plt.savefig(title + '_generated.png')
    plt.show()

def show_batch_subimg(imgs1, imgs2, title='', dim = (6,6)):
    plt.figure(figsize=dim)
    for i in range(imgs1.shape[0]):
        img1 = imgs1[i]
        img2 = imgs2[i]
        plt.subplot(dim[0], dim[1], i+1)
        plt.imshow(np.array(np.floor((img1/2 - img2/2)) + 128, dtype=np.uint8), interpolation='nearest')
        plt.axis('off')

    plt.suptitle(title)
    plt.tight_layout()
    plt.subplots_adjust(top=0.92, wspace=0.1, hspace=0.1)
    plt.savefig(title + '_generated.png')
    plt.show()

def show_dataset_summary(ds, dim=(11,10)):
    """
    ds:an instance of GelSightDataset
    """

    #color_maps = {'gray': plt.cm.gray_r, 'red': plt.cm.Reds}
    def set_text(text, x=0.0, y=1.1, color='green'):
        plt.text(x, y, text, color=color, alpha=0.7, size=8)

    plt.figure(figsize=dim)
    for i,e in enumerate(ds.table.items()):
        name = e[0]
        print(i, name)
        count = len(e[1])
        d = ds.load(e[1][0])
        img = d['kinectA_rgb_during']
        cimg = img[:,257:550,:]
        plt.subplot(dim[0], dim[1], i+1)
        plt.imshow(img, interpolation='nearest')
        #set_text(name, color='red')
        plt.title(name, fontsize=8)
        plt.legend(loc='lower right', fontsize=7, title=str(count))
        plt.axis('off')
    # plt.tight_layout()
    plt.subplots_adjust(wspace=0.1, hspace=0.5)
    plt.show()
    plt.savefig('summary.png')
