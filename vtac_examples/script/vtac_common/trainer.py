__author__ = "Ryo Hanai <ryo.hanai@aist.go.jp>"
__status__ = ""
__version__ = ""
__date__ = ""

import numpy as np
import time, datetime

from tensorflow import keras
from tensorflow.keras.models import load_model
from tensorflow.keras.optimizers import Adam
from tensorflow.keras.callbacks import TensorBoard, ModelCheckpoint
#from tf_explain.callbacks.vanilla_gradients import VanillaGradientsCallback

from . import generator

# def train_for_classification(ds,
#                                  model,
#                                  batch_size=16,
#                                  n_epochs=20,
#                                  rescale=1./255,
#                                  lr=1e-5,
#                                  model_file_name='model.hdf5'):

#     """
#     This function was used for screw direction recognition
#     """

#     prefix = model.name
#     dims = model.input.shape.dims
#     xshape = dims[1].value, dims[2].value, dims[3].value
#     num_classes = model.output.shape.dims[1].value

#     def logdir(prefix, batch_size, rescale):
#         now_str = datetime.datetime.now().strftime('%y%m%d%H%M%S')
#         r_suffix = 'n' if rescale == None else 'r'
#         return f'./log/{prefix}-bs{batch_size:02d}-{r_suffix}-{now_str}'

#     vtg = generator.VisTacGenerator(vertical_flip=True,
#                                         random_crop=list(xshape[:2]),
#                                         rescale=rescale)
#     train_gen = vtg.flow_from_HDFs(ds, batch_size=batch_size)

#     model.compile(loss='categorical_crossentropy', optimizer=Adam(lr=lr), metrics=['accuracy'])
#     model.summary()

#     tb_cb = TensorBoard(log_dir=logdir(prefix, batch_size, rescale),
#                             write_graph=True,
#                             write_images=1,
#                             )
#     md_cb = ModelCheckpoint(model_file_name,
#                             monitor='val_loss',
#                             verbose=1,
#                             save_best_only=True,
#                             save_weights_only=False)

#     xs,ys = ds.validation_data()
#     xs2 = []
#     for img in xs:
#         # center crop
#         height,width,_ = img.shape
#         dx,dy,_ = xshape
#         x = np.random.randint(0, width - dx + 1)
#         y = np.random.randint(0, height - dy + 1)
#         cropped_img = img[y:(y+dy), x:(x+dx), :]
#         # rescale
#         cropped_img = cropped_img * rescale
#         xs2.append(cropped_img)
#     val_data = np.array(xs2),np.array(ys)

#     model.fit_generator(train_gen, steps_per_epoch=train_gen.N//batch_size,
#         validation_data=val_data,
#         epochs=n_epochs,
#         callbacks=[tb_cb, md_cb]
#         )

def train_for_classification(ds,
                                 model,
                                 batch_size=16,
                                 n_epochs=20,
                                 rescale=1./255,
                                 lr=1e-5,
                                 save_best_only=False,
                                 model_file_name='model.hdf5'):

    prefix = model.name
    dims = model.input[0].shape.dims
    xshape = dims[1].value, dims[2].value, dims[3].value
    num_classes = model.output.shape.dims[1].value

    def logdir(prefix, batch_size, rescale):
        now_str = datetime.datetime.now().strftime('%y%m%d%H%M%S')
        r_suffix = 'n' if rescale == None else 'r'
        return f'./log/{prefix}-bs{batch_size:02d}-{r_suffix}-{now_str}'

    vtg = generator.VisTacGenerator(vertical_flip=True,
                                        horizontal_flip=True,
                                        rescale=rescale,
                                        width_shift_range=0.125,
                                        height_shift_range=0.125,
                                        rotation_range=10, # [degree]
                                        same_transform_to_tags=True,
                                        shuffle_input_tags=False
                                        )
    train_gen = vtg.flow_from_HDFs(ds, batch_size=batch_size)

    model.compile(loss='categorical_crossentropy', optimizer=Adam(lr=lr), metrics=['accuracy'])
    model.summary()

    tb_cb = TensorBoard(log_dir=logdir(prefix, batch_size, rescale),
                            write_graph=True,
                            write_images=1,
                            )

    if save_best_only:
        md_cb = ModelCheckpoint(model_file_name,
                                monitor='val_loss',
                                verbose=1,
                                save_best_only=True,
                                save_weights_only=False)
    else:
        md_cb = ModelCheckpoint(model_file_name,
                                monitor='val_loss',
                                verbose=1,
                                save_best_only=False,
                                save_weights_only=False,
                                save_freq='epoch')

    xs,ys = ds.validation_data()

    xs2 = []
    for i in range(dims[0].value):
        xs2.append(xs[i].astype(np.float32) * rescale)
    val_data = xs2, ys

    model.fit(train_gen, steps_per_epoch=train_gen.N//batch_size,
        validation_data=val_data,
        epochs=n_epochs,
        callbacks=[tb_cb, md_cb]
        )
