import numpy as np
import threading
from tensorflow.keras.preprocessing.image import ImageDataGenerator

#
# Implementation of a random cropping data generator.
# ImageDataGenerator of Keras does not support random cropping.
#
# cf.
#    https://jkjung-avt.github.io/keras-image-cropping/
#    https://qiita.com/koshian2/items/909360f50e3dd5922f32
#

#
# This generator loads data from multiple HDF files saved by deepdish,
# instead of loading from images stored in directories for object classification.
#

class Iterator(object):
    def __init__(self, N, batch_size, shuffle, seed):
        self.N = N
        self.batch_size = batch_size
        self.shuffle = shuffle
        self.batch_index = 0
        self.total_batches_seen = 0
        self.lock = threading.Lock()
        self.index_generator = self._flow_index(N, batch_size, shuffle, seed)

    def reset(self):
        self.batch_index = 0

    def _flow_index(self, N, batch_size=32, shuffle=False, seed=None):
        self.reset()
        while True:
            if self.batch_index == 0:
                index_array = np.arange(N)
                if shuffle:
                    if seed is not None:
                        np.random.seed(seed + self.total_batches_seen)
                    index_array = np.random.permutation(N)

            current_index = (self.batch_index * batch_size) % N
            if N >= current_index + batch_size:
                current_batch_size = batch_size
                self.batch_index += 1
            else:
                current_batch_size = N - current_index
                self.batch_index = 0
            self.total_batches_seen += 1
            yield (index_array[current_index: current_index + current_batch_size],
                       current_index, current_batch_size)

    def __iter__(self):
        return self

    def __next__(self, *args, **kwargs):
        return self.next(*args, **kwargs)

class HDFIterator(Iterator):
    def __init__(self, dataset, data_generator, object_names=None,
                     batch_size=32, shuffle=False, seed=None):
        self.ds = dataset
        self.data_generator = data_generator
        if object_names != None and len(object_names) > 0:
            self.indices = self.ds.index_list_by_object_names(object_names)
        else:
            self.indices = self.ds.get_ids_for_generation()

        self.tags = dataset.input_tags
        self.batch_index = 0
        self.total_batches_seen = 0
        super().__init__(len(self.indices), batch_size, shuffle, seed)

    def next(self, batch_size=32, shuffle=False, seed=None, with_data_id=False):
        tags = self.ds.input_tags
        with self.lock:
            index_array, current_index, current_batch_size = next(self.index_generator)

        batches_x = {}
        for tag in tags:
            Xshape = self.data_generator.random_crop_size + [self.ds.data_shape(tag)[2]] if self.data_generator.random_crop_size else list(self.ds.data_shape(tag))
            batches_x[tag] = np.zeros(tuple([current_batch_size] + Xshape))
        # batch_y = np.zeros((current_batch_size, 1)) # for 2-class classification
        batch_y = np.zeros((current_batch_size,
                                self.ds.data_shape(self.ds.output_tags[0])[0])) # for n-class classification

        for i, j in enumerate(index_array):
            e = self.ds.load(self.indices[j])

            # def f(x):
            #     x = self.random_transform(x.astype('float32'))
            #     x = self.random_crop(x)
            #     return x

            # Currently, the same transform is applied to all tags.
            # Thus, if the input data has different shapes for each tag, this function does not work.
            # It is better to apply separate transforms.

            if self.data_generator.same_transform_to_tags:
                # all the tags must have the same shape
                tf = self.data_generator.get_random_transform(img_shape=self.ds.input_shape())
                for tag in tags:
                    x = self.data_generator.apply_transform(e[tag].astype('float32'), tf)
                    batches_x[tag][i] = self.data_generator.standardize(x)
            else:
                if self.data_generator.shuffle_input_tags:
                    atags = np.array(tags)[np.random.permutation(6)]
                else:
                    atags = tags
                for tag,atag in zip(tags, atags):
                    tf = self.data_generator.get_random_transform(img_shape=self.ds.data_shape(tag))
                    x = self.data_generator.apply_transform(e[tag].astype('float32'), tf)
                    batches_x[atag][i] = self.data_generator.standardize(x)

            otag = self.ds.output_tags[0]
            batch_y[i] = e[otag]

        if with_data_id:
            return list(batches_x.values()), batch_y, [self.indices[i] for i in index_array]
        else:
            return list(batches_x.values()), batch_y

class VisTacGenerator(ImageDataGenerator):
    def __init__(self, featurewise_center = False, samplewise_center = False,
                 featurewise_std_normalization = False, samplewise_std_normalization = False,
                 zca_whitening = False, zca_epsilon = 1e-06, rotation_range = 0.0, width_shift_range = 0.0,
                 height_shift_range = 0.0, brightness_range = None, shear_range = 0.0, zoom_range = 0.0,
                 channel_shift_range = 0.0, fill_mode = 'nearest', cval = 0.0, horizontal_flip = False,
                 vertical_flip = False, rescale = None, preprocessing_function = None,
                 data_format = None, validation_split = 0.0, random_crop = None,
                 same_transform_to_tags = True,
                 shuffle_input_tags = False):

        super().__init__(featurewise_center, samplewise_center,
                             featurewise_std_normalization, samplewise_std_normalization,
                             zca_whitening, zca_epsilon, rotation_range, width_shift_range,
                             height_shift_range, brightness_range, shear_range, zoom_range,
                             channel_shift_range, fill_mode, cval, horizontal_flip,
                             vertical_flip, rescale, preprocessing_function,
                             data_format, validation_split)

        assert random_crop == None or len(random_crop) == 2
        self.random_crop_size = random_crop
        self.same_transform_to_tags = same_transform_to_tags
        self.shuffle_input_tags = shuffle_input_tags

    def flow_from_HDFs(self, dataset, object_names=None, batch_size=32, shuffle=True, seed=None):
        return HDFIterator(dataset, self, object_names, batch_size, shuffle, seed)

    def get_random_transform(self, img_shape, seed=None):
        tf = super().get_random_transform(img_shape, seed)
        if self.random_crop_size != None:
            height, width = img_shape[0], img_shape[1]
            dy, dx = self.random_crop_size
            if img_shape[0] < dy or img_shape[1] < dx:
                raise ValueError(f"Invalid random_crop_size : original = {img_shape}, crop_size = {self.random_crop_size}")

            x = np.random.randint(0, width - dx + 1)
            y = np.random.randint(0, height - dy + 1)
            tf['crop'] = [(x, y), (dx, dy)]
        return tf

    def apply_transform(self, img, transform_parameters):
        img = super().apply_transform(img, transform_parameters)
        try:
            (x,y),(dx,dy) = transform_parameters['crop']
            assert img.shape[2] == 3
            return img[y:(y+dy), x:(x+dx), :]
        except:
            return img
