'''
Dataset class
'''

__author__ = "Ryo Hanai <ryo.hanai@aist.go.jp>"
__status__ = ""
__version__ = ""
__date__ = ""

from abc import ABC, abstractmethod

class Dataset(ABC):
    """
    When you implement a class for your dataset,
    you define a class by inheriting Dataset class, and implement method called by generators.
    """

    @abstractmethod
    def get_ids_for_generation(self):
        pass

    @property
    def input_tags(self):
        return self._input_tags
    @input_tags.setter
    def input_tags(self, tags):
        self._input_tags = tags

    @property
    def output_tags(self):
        return self._output_tags
    @output_tags.setter
    def output_tags(self, tags):
        self._output_tags = tags

    @abstractmethod
    def data_shape(self, tag):
        pass

    @abstractmethod
    def input_shape(self):
        pass

    @property
    def n_classes(self):
        return self._n_classes
    @property
    def n_inputs(self):
        return len(self._input_tags)

    @abstractmethod
    def load(self, data_id):
        pass

    @abstractmethod
    def validation_data(self):
        pass
