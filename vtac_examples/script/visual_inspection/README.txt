＊デモプログラムの実行方法

$ roscd vtac_examples/script/visual_inspection
$ ipython3 demo191106.py -i
In [1]: demo = Demo()
In [2]: demo.set_sub_problem(0)
ここで、モデルのロードに時間がかかります。
In [3]: demo.predict()

set_sub_problemの番号を変えてpredictを実行すると、
別のデータ分割で学習したモデルを用いてpredictします。


＊データ（アーカイブ）

~/.keras/datasets/vinspectdata191001.h5
　10/01に届いたデータ
~/.keras/datasets/vinspectdata191011.h5
　10/11に届いたデータ


＊アーカイブの構造

1つのデータ（元データのset）が辞書形式の1エントリ
{'id':<適当につけた整数>,
 'background':<背景を表す整数>,
 'cls':<欠陥か汚れかを表す整数>,
 'sub_cls':<欠陥や汚れの種類>, // 最初のデータで○打痕が少なかったので、打痕と○打痕を同じ番号に割当てました
 'set_number':<set番号>,
 'image00':<view0の画像データ>,
  ...
 'image06':<view1の画像データ>
 }

中身を見るには、
$ ipython3 vinspect.py -i
ds = VInspectDataset('191001')

ds._ids # 適当につけられたデータのID一覧
ds._data # アーカイブの生データ（上記辞書形式のリスト）

ds.info(11) # データの情報
from vtac_common import visualization
visualization.show_image(ds.load(11)['image00']) # 画像の表示


＊ソースの構造

ディレクトリ　vtac_examples/script/visual_inspection

vinspect_data.py：アーカイブの作成と読込み
　アーカイブの作成はデータが変わらなければ、最初の一回だけでよい
　　メールで送付されたディレクトリ構造から上記の .h5 ファイルを作成します
　　作成したアーカイブが ~/.keras/datasetsにあるもの

vinspect.py：データセットクラス
　アーカイブへアクセスするインタフェースを提供するクラス
　　どのアーカイブを使用するかをインスタンス生成時に引数で指定する
　　識別問題の定義もこのクラスで行っている
　set_problem()で問題を指定
　　problemとは、
　　　- 11/01のデータを使う
　　　- 入力は image00,...,image02,image04,...,image06 の7つのview
　　　- 出力は 欠陥 or 汚れ の2クラス
　　　などの問題設定をまとめたもの
　set_sub_problem()でサブ問題番号を指定
　　sub-problemとは、
　　　- problemが決まったときのデータ分割の仕方を表します。
　　　- 例えば、problem='191001_defect_or_dirt'はleave-one-outで評価を行うため、
　　　　sub-problem番号を0-22まで変えながら23回学習させるという使い方をします。

vinspect_test.py：学習実験プログラム
　model_vinspect_max：今回の実験で使ったネットワーク
　train_vinspect191001
　train_vinspect191011
　　sub-problemに対して学習を行うプログラム
　　　問題の設定、モデルの作成、学習を行う

demo191106.py：識別デモ用のクラス


ディレクトリ　vtac_examples/script/vtac_common

dataset.py：データセットの抽象クラス
　generatorに対するインタフェースを提供する
　自分のデータセットを定義するときはこのインタフェースを実装する

generator.py：学習時にバッチを生成するクラス
　データセットクラスのインスタンスを使う
　データ拡張や入力の正規化等を行う

trainer.py：学習処理を記述するクラス
　メモリに載らないくらいデータサイズが大きくなったことを想定してgeneratorを使用しています
　データ拡張のパラメータ設定、lossやoptimizerの設定はここにあります
　（これはvinspect_test.pyに持っていったほうがよいかもしれない）

visualization.py：画像の可視化を行うクラス
