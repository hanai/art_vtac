#!/usr/bin/env python3

__author__ = "Ryo Hanai <ryo.hanai@aist.go.jp>"
__status__ = ""
__version__ = ""
__date__ = ""

from vinspect import *
from vtac_common import visualization

import numpy as np
import matplotlib.pyplot as plt
import keras
from keras.models import load_model

from keras.layers import Input, Subtract, Dense, Conv2D
from keras.layers import Concatenate, Add, Maximum
from keras.layers import Flatten, GlobalAveragePooling2D, MaxPooling2D
from keras.applications.resnet50 import ResNet50

np.set_printoptions(precision=6, suppress=True)

#ds = VInspectDataset(dataset_key='191001')
#problem = '191001_defect_or_dirt'

def show_views(id):
    d = ds.load(id)
    visualization.show_batch(np.array([d['image%02d'%i] for i in list(range(3))+list(range(4,7))]),
                                 title=str(ds.info(id, lang='en')))

class Demo:
    """
    Usage:
    demo = Demo()
    demo.set_sub_problem(0) # sub problem number: 0, ..., 5
    demo.predict()
    """

    def __init__(self):
        self.problem = '191011_defect_type4'
        self.ds = VInspectDataset(dataset_key='191011')
        self.ds.set_problem(self.problem)

    def set_sub_problem(self, n):
        self.ds.set_sub_problem(n)
        modelfile = 'model.%s.%d.hdf5' % (self.problem, n)
        print('loading {} ...'.format(modelfile))
        self.model = load_model(modelfile)
        print('finished.')

    def do_predict(self):
        vids = self.ds.test_ids()
        vd = self.ds.validation_data()
        rescaled_input = []
        for i in range(6):
            rescaled_input.append(vd[0][i] * 1./255)
        y = self.model.predict(rescaled_input)
        return vids, y

    def predict(self):
        vd, y = self.do_predict()
        self.show_result(vd, y)

    def show_result(self, vids, y):
        cols = 6
        dim = (len(vids), cols)
        plt.figure(figsize=dim)

        def subplot(img, j):
            plt.subplot(dim[0], dim[1], j)
            plt.imshow(img)
            plt.axis('off')

        for i, vid in enumerate(vids):
            d = self.ds.load(vid)
            for j in range(cols):
                subplot(d['image%02d'% (j if j < 3 else j+1)], cols*i+j+1)
                if j == 3:
                    msg = '{} {} {} {}'.format(self.ds.info(vid, lang='en'), y[i], d['sub_cls'],
                                            np.argmax(y[i]) == np.argmax(d['sub_cls']))
                    plt.title(msg)

        plt.tight_layout()
        plt.subplots_adjust(top=0.96, bottom=0.02, wspace=0.3, hspace=0.3)
        plt.show()

