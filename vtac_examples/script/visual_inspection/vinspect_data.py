# -*- coding: utf-8 -*-

__author__ = "Ryo Hanai <ryo.hanai@aist.go.jp>"
__status__ = ""
__version__ = ""
__date__ = ""

import os, copy
import os.path
import numpy as np
import deepdish as dd

from tensorflow.keras.preprocessing import image
from tensorflow.keras.utils import get_file
#from PIL import ImageOps, Image

import glob, re
from enum import Enum


background_code = {'加工面': 0, '鋳肌面': 1, '未定義': 2}
background_code_e = {'kako-men': 0, 'ihada-men': 1, 'undefined': 2}
class_code = {'欠陥': 0, '汚れ': 1}
class_code_e = {'kekkan': 0, 'yogore': 1}
subclass_code = {'BH': 0, '鋳巣': 1, 'キズ': 2, '打痕': 3, '○打痕': 3, '湯しわ': 4, '油固着': 5, '未定義': 6}
subclass_code_e = {'BH': 0, 'isou': 1, 'kizu': 2, 'dakon': 3, 'maru-dakon': 3, 'yushiwa': 4, 'abura-kotyaku': 5, 'undefined': 6}

def encode(set_desc):
    return background_code[set_desc[0]], class_code[set_desc[1]], subclass_code[set_desc[2]]

def decode_bg(code, lang='jp'):
    if lang == 'jp':
        return [k for k,v in background_code.items() if v == code]
    else:
        return [k for k,v in background_code_e.items() if v == code]
def decode_cls(code, lang='jp'):
    if lang == 'jp':
        return [k for k,v in class_code.items() if v == code]
    else:
        return [k for k,v in class_code_e.items() if v == code]

def decode_subcls(code, lang='jp'):
    if lang == 'jp':
        return [k for k,v in subclass_code.items() if v == code]
    else:
        return [k for k,v in subclass_code_e.items() if v == code]

def create_archive_2019_10_01(dir_path='/home/ryo/Downloads/data191001/',
                                  out_file='vinspectdata191001.h5'):
    data = []
    set_dirs = glob.glob(os.path.join(dir_path + '*/*/*/32x32'))

    for set_id, set_dir in enumerate(set_dirs):
        set_desc = re.findall(dir_path+'(.*)/set*', set_dir)[0].split('_')
        bgcode,clscode,subclscode = encode(set_desc)
        set_num = int(re.findall(dir_path+'.*/set(.*)/main', set_dir)[0])
        imgfiles = os.listdir(set_dir)
        d = {
            'id': set_id,
            'background': bgcode,
            'cls': clscode,
            'sub_cls': subclscode,
            'set_number': set_num,
            }
        for imgfile in imgfiles:
            filepath = os.path.join(set_dir, imgfile)
            img = image.load_img(filepath)
            view_number = int(imgfile[0])
            d['image%02d'%view_number] = np.array(img)
        data.append(d)

    print('saving to', out_file)
    dd.io.save(out_file, data)

def create_archive_2019_10_11(dir_path='/home/ryo/Downloads/data191011/',
                                  out_file='vinspectdata191011.h5'):
    data = []
    set_dirs = glob.glob(os.path.join(dir_path + '*/*/*/32x32'))

    for set_id, set_dir in enumerate(set_dirs):
        set_desc = re.findall(dir_path+'(.*)/.*_set*', set_dir)[0].split('_')
        set_tag = re.findall(dir_path+'.*/([0-9]*_set[0-9])/', set_dir)[0]
        bgcode,clscode,subclscode = encode(set_desc)
        set_num = [int(s) for s in set_tag.split('_set')]
        imgfiles = os.listdir(set_dir)
        d = {
            'id': set_id,
            'background': bgcode,
            'cls': clscode,
            'sub_cls': subclscode,
            'set_number': set_num,
            }
        for imgfile in imgfiles:
            filepath = os.path.join(set_dir, imgfile)
            img = image.load_img(filepath)
            view_number = int(imgfile[0])
            d['image%02d'%view_number] = np.array(img)
        data.append(d)

    print('saving to', out_file)
    dd.io.save(out_file, data)

def create_archive_2020_01_20(dir_path='/home/ryo/Downloads/data200120/',
                                  out_file='vinspectdata200120.h5'):
    data = []
    set_dirs = glob.glob(os.path.join(dir_path, '*/*/*/'))

    for set_id, set_dir in enumerate(set_dirs):
        print(set_dir)
        tag,nstr = re.findall(os.path.join(dir_path, '.*/(\D+)(\d+)'), set_dir)[0]
        if tag == '打痕':
            set_desc = '未定義', '欠陥', tag
        elif tag == '鋳巣':
            set_desc = '未定義', '欠陥', tag
        elif tag == '汚れ':
            set_desc = '未定義', '汚れ', 'キズ'
        else:
            print('unexpected tag: ', tag)
            return

        bgcode,clscode,subclscode = encode(set_desc)
        set_num = int(nstr)
        imgfiles = glob.glob(os.path.join(set_dir, '*.bmp'))
        d = {
            'id': set_id,
            'background': bgcode,
            'cls': clscode,
            'sub_cls': subclscode,
            'set_number': set_num,
            }
        for imgfile in imgfiles:
            img = image.load_img(imgfile)
            view_number = int(re.findall('.*/(\d+)_.*', imgfile)[0])
            d['image%02d'%view_number] = np.array(img)
        data.append(d)

    print('saving to', out_file)
    dd.io.save(out_file, data)


archives = {
    '191001' : ('vinspectdata191001.h5', 'https://drive.google.com/uc?export=download&id=1AQbJsNppvdq6-b_RU2IqwptAfNpG-B9i'),
    '191011' : ('vinspectdata191011.h5', ''),
    '200120' : ('vinspectdata200120.h5', ''),
    }

def load_data(key):
    """
    Load *.h5 archive from ~/.keras/datasets directory if the archive exists.
    If not this function download the archive from the Internet

    @param path An archive file name

    @return Loaded data

    """

    path, origin = archives[key]
    path = get_file(path, origin = origin)

    print('Loading file: %s' % path)
    data = dd.io.load(path)
    print("N data: %d" % len(data))
    return data
