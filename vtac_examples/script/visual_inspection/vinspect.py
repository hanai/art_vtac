#!/usr/bin/env python3

__author__ = "Ryo Hanai <ryo.hanai@aist.go.jp>"
__status__ = ""
__version__ = ""
__date__ = ""

import numpy as np
import time, math, datetime

from tensorflow import keras
from tensorflow.keras.models import load_model
from tensorflow.keras.optimizers import Adam
from tensorflow.keras.callbacks import TensorBoard, ModelCheckpoint

from vtac_common import dataset
import vinspect_data

class VInspectDataset(dataset.Dataset):
    """
    dataset_key = '191001' | '191011' | '200120'
    """

    def __init__(self, dataset_key='191011'):
        # super().__init__()
        self.scan_files(dataset_key)

    def get_ids_for_generation(self):
        return self.train_ids()

    def load(self, data_id):
        return self._data[self._table[data_id]]

    def data_shape(self, tag):
        return self._data[0][tag].shape

    def input_shape(self):
        return self.data_shape(self.input_tags[0])

    def validation_data(self):
        itags = self.input_tags
        otag = self.output_tags[0]
        xs = []
        for itag in itags:
            xs.append(np.zeros([len(self.test_ids())] + list(self.data_shape(itag))))
        ys = np.zeros([len(self.test_ids())] + list(self.data_shape(otag)))
        for j,did in enumerate(self.test_ids()):
            d = self.load(did)
            for i,itag in enumerate(itags):
                xs[i][j] = d[itag]
            ys[j] = d[otag]
        return xs,ys

    def scan_files(self, dataset_key):
        self._table = {}
        self._data = vinspect_data.load_data(dataset_key)
        for i,d in enumerate(self._data):
            self._table[d['id']] = i

    def set_problem(self, problem, seed=1337):
        self._problem = problem
        if problem == '191001_defect_or_dirt':
            self.input_tags = ['image00', 'image01', 'image02', 'image04', 'image05', 'image06']
            self.output_tags = ['cls']
            self._n_classes = 2
            otag = self.output_tags[0]
            for i,d in enumerate(self._data):
                d[otag] = keras.utils.to_categorical(d[otag], self._n_classes)
        elif problem == '191011_defect_type4':
            #self.input_tags = ['image00', 'image01', 'image02', 'image03', 'image04', 'image05', 'image06']
            self.input_tags = ['image00', 'image01', 'image02', 'image04', 'image05', 'image06']
            self.output_tags = ['sub_cls']
            self._n_classes = 2
            otag = self.output_tags[0]
            for i,d in enumerate(self._data):
                d[otag] = keras.utils.to_categorical(d[otag]-2, self._n_classes)
        elif problem == '200120_dent_casthole_dirt':
            self.input_tags = ['image{:02d}'.format(d) for d in range(1,26) if d != 10]
            self.output_tags = ['new_cls']
            self._n_classes = 3
            otag = self.output_tags[0]
            for i,d in enumerate(self._data):
                if (d['cls'],d['sub_cls']) == (0,3):
                    d[otag] = 0
                elif (d['cls'],d['sub_cls']) == (0,1):
                    d[otag] = 1
                elif (d['cls'],d['sub_cls']) == (1,2):
                    d[otag] = 2
                d[otag] = keras.utils.to_categorical(d[otag], self._n_classes)

        self.shuffle_and_split_ids(k=5, seed=seed)

    def set_sub_problem(self, number):
        if self._problem == '191001_defect_or_dirt':
            l = len(self._data)
            if number < 0 or number >= l:
                print('problem number must be between {} and {}'.format(0, l))
                return

            self._test_ids = [self._ids[number]]
            self._train_ids = np.concatenate([self._ids[:number], self._ids[number+1:]])
        elif self._problem == '191011_defect_type4' or self._problem == '200120_dent_casthole_dirt':
            l = len(self._subgroup_ids)
            if number < 0 or number >= l:
                print('problem number must be between {} and {}'.format(0, l))
                return

            # Data are split into k sub-groups for k-split cross validation.
            # 4 of them are used for training, and the rest is used for test.
            # The problem number specifies which sub-group is used for test.
            self._train_ids = np.concatenate(self._subgroup_ids[:number]+self._subgroup_ids[number+1:])
            self._test_ids = self._subgroup_ids[number]

        print('train ids: ', self.train_ids())
        print('test ids: ', self.test_ids())

    def shuffle_and_split_ids(self, k, seed):
        np.random.seed(seed) # fix the seed of random number for the reproducibility
        ids = [d['id'] for d in self._data]
        self._subgroup_ids = []
        l = len(ids)
        shuffled_ids = np.random.permutation(l)
        self._ids = np.array(list(ids))[shuffled_ids]

        for i in range(0,k):
            s = l * i // k
            e = l * (i+1) // k
            self._subgroup_ids.append(np.array(list(ids))[shuffled_ids[s:e]])
            print("SUBGROUP[%d]:"%i, self._subgroup_ids[i])

    def train_ids(self):
        return self._train_ids

    def test_ids(self, class_number=None):
        return self._test_ids

    def info(self, data_id, lang='jp'):
        d = self.load(data_id)
        clscd = d['cls']
        if type(clscd) == np.ndarray:
            clscd = np.argmax(clscd)
        subclscd = d['sub_cls']
        if type(subclscd) == np.ndarray:
            subclscd = np.argmax(subclscd)

        if self._problem == '200120_dent_casthole_dirt':
            newcls = np.argmax(d['new_cls'])
            if newcls == 0:
                clsname = 'dakon'
            elif newcls == 1:
                clsname = 'isu'
            else:
                clsname = 'yogore'
            return data_id, clsname, 'set{}'.format(d['set_number'])
        else:
            if self._problem == '191011_defect_type4':
                subclscd = subclscd + 2 # this workaround is not good
            return data_id, vinspect_data.decode_bg(d['background'], lang=lang), vinspect_data.decode_cls(clscd, lang=lang), vinspect_data.decode_subcls(subclscd, lang=lang), 'set{}'.format(d['set_number'])
