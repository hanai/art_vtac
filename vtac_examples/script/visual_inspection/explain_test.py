import tensorflow as tf
from demo200120 import *
from vtac_common import visualization

class Explainer:
    """
    ex = Explaniner()
    ex.set_sub_problem(0)
    ex.explain(class_index=0, validation_index=0)
    ex.show(view_id=0)
    """

    def __init__(self):
        self.problem = '200120_dent_casthole_dirt'
        self.ds = VInspectDataset(dataset_key='200120')
        self.ds.set_problem(self.problem)

    def set_sub_problem(self, n):
        self.ds.set_sub_problem(n)
        self.xs,self.ys = self.ds.validation_data()
        modelfile = 'model.%s.%d.hdf5' % (self.problem, n)
        print('loading {} ...'.format(modelfile))
        self.model = load_model(modelfile)
        print('finished.')

    def explain(self, class_index=0, validation_index=0):
        self.validation_index = validation_index
        num_classes = self.model.output.shape[1]
        expected_output = tf.one_hot([class_index] * 1, num_classes)
        rescaled_input = []
        for i in range(len(self.xs)): # size of views
            rescaled_input.append(np.expand_dims(self.xs[i][self.validation_index] * 1./255, 0))

        with tf.GradientTape() as tape:
            inputs = []
            for i in range(len(self.xs)):
                ip = tf.cast(rescaled_input[i], tf.float32)
                inputs.append(ip)
                tape.watch(ip)
            predictions = self.model(inputs)
            loss = tf.keras.losses.categorical_crossentropy(
                expected_output, predictions
            )

        self.grad = tape.gradient(loss, inputs)

    def show(self, view_id=0, sum_rgb=True):
        a = np.array(self.grad[view_id][0])
        if sum_rgb:
            a = np.sum(a, axis=2)
        b = np.abs(a)
        s = np.max(b)
        print('s=', s)
        if s > 0.001:
            visualization.show_heatmap(b/s)
        else:
            print('zero gradients')
            return

        visualization.show_image(self.xs[view_id][self.validation_index])
