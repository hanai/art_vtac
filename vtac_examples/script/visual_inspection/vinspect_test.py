#!/usr/bin/env python3

__author__ = "Ryo Hanai <ryo.hanai@aist.go.jp>"
__status__ = ""
__version__ = ""
__date__ = ""

from vinspect import *
from vtac_common import trainer

import numpy as np
from tensorflow import keras
import tensorflow.keras.backend as K
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Input, Subtract, Concatenate, Dense, Conv2D, Add, Maximum
from tensorflow.keras.layers import Flatten, GlobalAveragePooling2D, MaxPooling2D
from tensorflow.keras.applications.resnet50 import ResNet50
import tensorflow as tf

##
## define NN-models
##

def model_vinspect_max(n_views, input_shape, n_classes, n_mid_params=16):
    """
    n_mid_params = 16, ..., 256
    """

    imgs = []
    for i in range(n_views):
        imgs.append(Input(shape=input_shape))
    with tf.name_scope('ResNet50'):
        resnet = ResNet50(include_top=False, input_shape=input_shape, weights='imagenet')

    feats = []
    for i in range(n_views):
        x = resnet(imgs[i])
        x = GlobalAveragePooling2D()(x)
        feats.append(x)
    x = Maximum()(feats)
    # x = Flatten()(x)
    x = Dense(n_mid_params, activation='relu')(x)
    success_rate = Dense(n_classes, activation='softmax')(x)
    return keras.models.Model(inputs=imgs, outputs=[success_rate], name='visual_inspection')

def model_vinspect_add(n_views, input_shape, n_classes, n_mid_params=16):
    imgs = []
    for i in range(n_views):
        imgs.append(Input(shape=input_shape))
    with tf.name_scope('ResNet50'):
        resnet = ResNet50(include_top=False, input_shape=input_shape, weights='imagenet')

    feats = []
    for i in range(n_views):
        feats.append(resnet(imgs[i]))
    x = Add()(feats)
    x = Flatten()(x)
    x = Dense(n_mid_params, activation='relu')(x)
    success_rate = Dense(n_classes, activation='softmax')(x)
    return keras.models.Model(inputs=imgs, outputs=[success_rate], name='visual_inspection')

def model_vinspect_concat(n_views, input_shape, n_classes, n_mid_params=16):
    imgs = []
    for i in range(n_views):
        imgs.append(Input(shape=input_shape))
    with tf.name_scope('ResNet50'):
        resnet = ResNet50(include_top=False, input_shape=input_shape, weights='imagenet')

    feats = []
    for i in range(n_views):
        feats.append(resnet(imgs[i]))
    channel_axis = 1 if K.image_data_format() == 'channels_first' else 3
    x = Concatenate(axis=channel_axis)(feats)
    x = GlobalAveragePooling2D()(x)
    x = Dense(256, activation='relu')(x)
    success_rate = Dense(num_classes, activation='softmax')(x)
    return keras.models.Model(inputs=imgs, outputs=[success_rate], name='visual_inspection')

def model_vinspect_sub(n_views, n_classes, input_shape=(32, 32, 3)):
    imgs = []
    for i in range(n_views):
        imgs.append(Input(shape=input_shape))
    subs = []
    for i in range(n_views-1):
        subs.append(Subtract()([imgs[i], imgs[i+1]]))

    with tf.name_scope('ResNet50'):
        resnet = ResNet50(include_top=False, input_shape=input_shape, weights='imagenet')

    feats = []
    for i in range(n_views-1):
        feats.append(resnet(subs[i]))
    x = Add()(feats)
    x = Flatten()(x)
    x = Dense(16, activation='relu')(x)
    success_rate = Dense(num_classes, activation='softmax')(x)
    return keras.models.Model(inputs=imgs, outputs=[success_rate], name='visual_inspection')

def CNN1(input_shape):
    img_input = Input(shape=input_shape)
    x = Conv2D(32, kernel_size=(3,3))(img_input)
    x = BatchNormalization()(x)
    x = Activation('relu')(x)
    x = Conv2D(64, kernel_size=(3,3))(x)
    x = BatchNormalization()(x)
    x = Activation('relu')(x)
    model = Model(img_input, x, name='CNN')
    return model

def model_vinspect_cnn(n_views, n_classes):
    """
    not yet implemented
    """

    xshape = (32,32,3)

    # setup dataset instance used for training
    ds = VInspectDataset(seed=seed)
    ds.set_problem_number(problem_number)
    num_classes = ds.num_classes()

    # design my model used for training
    n_views = 6
    imgs = []
    for i in range(n_views):
        imgs.append(Input(shape=xshape))

    with tf.name_scope('CNN'):
        cnn = CNN1()

    feats = []
    for i in range(n_views):
        feats.append(cnn(imgs[i]))
    x = Maximum()(feats)
    x = Flatten()(x)
    #x = Dense(256, activation='relu')(x)
    #x = Dense(16, activation='relu')(x)
    success_rate = Dense(num_classes, activation='softmax')(x)
    model = keras.models.Model(inputs=imgs, outputs=[success_rate], name='visual_inspection')

    # train the model for N-class classification problem
    trainer.train_for_classification2(ds,
                                         model,
                                         batch_size=batch_size,
                                         n_epochs=n_epochs,
                                         lr=lr,
                                         model_file_name='model.prob%d.hdf5' % problem_number)


def train_vinspect191001(sub_problem_number, batch_size=16, n_epochs=20, lr=1e-5, seed=1337):
    """
    seed: Seed of random number used to split data for training and test
    """

    # Instantiate dataset and configure it.
    # Problem defines dataset used, input, output, etc.
    # Sub-problem defines the data split for training and test.
    # Both functions configure the dataset instance.

    ds = VInspectDataset(dataset_key='191001')
    problem = '191001_defect_or_dirt'
    ds.set_problem(problem, seed=seed)
    ds.set_sub_problem(sub_problem_number)

    # Define model used for training
    model = model_vinspect_max(n_views=ds.n_inputs, input_shape=ds.input_shape(), n_classes=ds.n_classes)

    # train the model for N-class classification problem
    trainer.train_for_classification(ds, model,
                                         batch_size=batch_size,
                                         n_epochs=n_epochs,
                                         lr=lr,
                                         model_file_name='model.%s.%d.hdf5' % (problem, sub_problem_number))

def train_vinspect191011(sub_problem_number, batch_size=16, n_epochs=20, lr=1e-5, seed=1337):
    ds = VInspectDataset(dataset_key='191011')
    problem = '191011_defect_type4'
    ds.set_problem(problem, seed=seed)
    ds.set_sub_problem(sub_problem_number)

    model = model_vinspect_max(n_views=ds.n_inputs, input_shape=ds.input_shape(), n_classes=ds.n_classes)

    trainer.train_for_classification(ds, model,
                                         batch_size=batch_size,
                                         n_epochs=n_epochs,
                                         lr=lr,
                                         model_file_name='model.%s.%d.hdf5' % (problem, sub_problem_number))

def train_vinspect200120(sub_problem_number, batch_size=16, n_epochs=20, lr=1e-5, seed=1337):
    ds = VInspectDataset(dataset_key='200120')
    problem = '200120_dent_casthole_dirt'
    ds.set_problem(problem, seed=seed)
    ds.set_sub_problem(sub_problem_number)

    model = model_vinspect_max(n_views=ds.n_inputs, input_shape=ds.input_shape(), n_classes=ds.n_classes)

    trainer.train_for_classification(ds, model,
                                         batch_size=batch_size,
                                         n_epochs=n_epochs,
                                         lr=lr,
                                         model_file_name='model.%s.%d.hdf5' % (problem, sub_problem_number))


import sys
import optparse

if __name__ == '__main__':
    parser = optparse.OptionParser()
    parser.add_option('-p', '--problem', dest='problem', type='string', help='')
    parser.add_option('-s', '--sub_problem_number', dest='spnum', type='int', default=0, help='')
    parser.add_option('-e', '--n_epochs', dest='n_epochs', type='int', default=100, help='')
    parser.add_option('-r', '--lr', dest='lr', type='float', default=1e-5, help='')
    parser.add_option('-v', dest='validation', action='store_true', help='')
    options, args = parser.parse_args(sys.argv[1:])

    problem = options.problem
    n_epochs = options.n_epochs
    lr = options.lr
    validation = options.validation

    print('problem = ', problem)
    print('number of epochs = ', n_epochs)
    print('learning rate = ', lr)

    if problem == '191001_defect_or_dirt':
        if validation:
            sub_problem_numbers = list(range(23))
        else:
            sub_problem_numbers = [options.spnum]
        train_f = train_vinspect191001
    elif problem == '191011_defect_type4':
        if validation:
            sub_problem_numbers = list(range(5))
        else:
            sub_problem_numbers = [options.spnum]
        train_f = train_vinspect191011
    elif problem == '200120_dent_casthole_dirt':
        if validation:
            sub_problem_numbers = list(range(5))
        else:
            sub_problem_numbers = [options.spnum]
        train_f = train_vinspect191011
    else:
        print('unknown problem')
        sys.exit(1)

    for i in sub_problem_numbers:
        train_f(i, batch_size=12, n_epochs=n_epochs, lr=lr)
